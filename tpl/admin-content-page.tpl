{include file=_top.tpl}

<script type="text/javascript" src="{$lib}tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  tinyMCE.init({ldelim}
      elements : "text",

      {include file='tiny_mce.tpl'}

  {rdelim});

  {include file='file_manager.tpl'}

  function generateAlias() {ldelim}

	var title = $F('title');
	title = title.toLowerCase();



	abeceda = new Array("a","-","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","r","s","t","u","v","w","x","y","z","0","1","2","3","4","5","6","7","8","9");

	patterns = new Array(" ","  ","ľ","š","č","ť","ž","ý","á","í","é","ó","ú","ĺ","ŕ","ř","ô","ě");
	reps     = new Array("-", "-","l","s","c","t","z","y","a","i","e","o","u","l","r","r","o","e");




	for (var i=0;i<patterns.length;i++)
		title = title.replace(new RegExp(patterns[i],"g"),reps[i]);

	newtitle="";

	for (var i=0;i<title.length;i++) {ldelim}
		if (abeceda.find(title[i])!=false)
			newtitle = newtitle+title[i];

	{rdelim}



	$('alias').value=newtitle;

  {rdelim}

  {literal}
  Array.prototype.find = function(searchStr) {
  var returnArray = false;
  for (i=0; i<this.length; i++) {
    if (typeof(searchStr) == 'function') {
      if (searchStr.test(this[i])) {
        if (!returnArray) { returnArray = [] }
        returnArray.push(i);
      }
    } else {
      if (this[i]===searchStr) {
        if (!returnArray) { returnArray = [] }
        returnArray.push(i);
      }
    }
  }
  return returnArray;
}
{/literal}


  function validateForm() {ldelim}
      var elem;
      var errs = 0;


      if (!validatePresent($('title'), 'title_i')) errs += 1;
      if (!validatePresent($('alias'), 'alias_i' )) errs += 1;


      if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}


  //-->
</script>

<div class="cms-tabs">
  {if $arg.2!="add-page"}<div>  <a href="{"content/`$content.alias`"|rA}">{"show"|t}</a></div>{/if}
  <div class="selected" >  {if $arg.2!="add-page"}<a href="{"admin/content/edit-page/`$content.id`"|rA}">{else}&nbsp;{/if} {"edit"|t}{if $arg.2!="add-page"}</a>{else}&nbsp;{/if}</div>
  <div> <a href="{"admin/inset-images/page"|rA}">{"inset_images"|t}</a></div>
  {if $arg.2!="add-page"}<div> <a href="{"admin/content/del-page/`$content.id`"|rA}" onclick="javascript:return confirm('{"content_page_delete"|t}?')">{"delete"|t}</a></div>{/if}
  <div><a href="{"admin/content"|rA}">{"show_all"|t}</a></div>

 <div class="clear"></div>
</div>

<h1>{if $content.add}{"content_newpage"|t}{else}{$content.title}{/if}</h1>

{if $content.add}
<p>
{"content_addtext"|t}
</p>
{/if}

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}

<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()">
    <div class="input">
      <label>{"title"|t}:</label>
      <input name="title" id="title" value="{$content.title}"
       onchange="validatePresent($('title'), 'title_i'); generateAlias();"/>
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>
{if $content.add && $languages}
    <div class="input">
      <label>{"language"|t}:</label>
      <select name="lang">
{foreach from=$languages item=i key=l}
        <option value="{$l}"{if $l == $content.lang} selected="selected"{/if}>{$i.name}</option>
{/foreach}
      </select>
    </div>
    {*<div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_lang3"|t}
      </div>
    </div>*}
{elseif $languages}
    <div class="input">
      <label>{"language"|t}:</label>
      <input value="{$content.language}" readonly="readonly" />
      <input value="{$content.language}" type="hidden" name="lang" />
    </div>
  {*  <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_lang2"|t}
      </div>
    </div>*}
{/if}
    <div class="input">
      <label>{"menu"|t}:</label>
      <select name="menu_id">
{foreach from=$menu_tree item=i}
        <option value="{$i.id}"{if $i.id == $content.menu_id} selected="selected"{/if}>{section name=indent start=0 loop=$i.depth} -{/section} {t arr=$i.title}</option>
{/foreach}
      </select>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{if $content.add}
{"content_belong"|t}
{else}
{"content_edittext"|t}
{/if}
      </div>
    </div>
    <div class="input">
      <label>{"menu_item"|t}:</label>
      <input name="menu" value="{$content.menu}" />
      <span class="info_nreq">{"forms_opt"|t}</span>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_ttext"|t}
      </div>
    </div>

        <div class="input">
      <label>{"alias"|t}</label>
      <input name="alias" id="alias" value="{$content.alias}" onchange="validatePresent($('alias'), 'alias_i')"/>
      <span id="alias_i" class="info">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_alias"|t}
      </div>
    </div>

    <div class="input">
      <label>{"content_album"|t}:</label>
      <select name="album_id">
      <option value="">{"non"|t}</option>
{foreach from=$albums item=i}
        <option value="{$i.id}"{if $i.id == $content.album_id} selected="selected"{/if}>{$i.title}</option>
{/foreach}
      </select>
    </div>

    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_album_text"|t}
      </div>
    </div>


<input type="hidden" name="add" value="{$content.add}" />

    <div class="inn">&nbsp;</div>

    <div class="input">
      <label>{"text"|t}:</label>
      <span class="info">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <textarea name="text" id="text" cols="50" rows="25" class="wysiwyg">{$content.text}</textarea>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{include file=_bottom.tpl}

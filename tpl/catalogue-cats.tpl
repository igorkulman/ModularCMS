{include file=_top.tpl}

{if $access.editor}{* admin links *}
<div class="cms-tabs">
  <div class="selected" >  <a href="{"catalogue"|rA}">{"tab_catalogue"|t}</a></div>
  <div>  <a href="{"admin/catalogue"|rA}">{"tab_catalogue_admin"|t}</a></div>
 <div class="clear"></div>
</div>
{/if}{* admin links *}

<h2>{if $lang=="sk"}Katalóg produktov{else}Products catalogue{/if}</h2>



{$categories->render()}



{include file=_bottom.tpl}

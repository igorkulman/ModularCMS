{include file=_top.tpl}

{if $access.editor}{* admin links *}
{*<div class="admin-link">
  [ <a href="{"admin/content/edit-page/`$content.id`"|rA}">{"edit"|t}</a>
  | <a href="{"admin/inset-images/page"|rA}">{"inset_images"|t}</a>
  | <a href="{"admin/content/del-page/`$content.id`"|rA}" onclick="javascript:return confirm('{"content_page_delete"|t}?')">{"delete"|t}</a>
  | <a href="{"admin/content"|rA}">{"show_all"|t}</a>
  ]
</div>*}
<div class="cms-tabs">
  <div class="selected" >  <a href="{"admin/content/`$content.alias`"|rA}">{"show"|t}</a></div>
  <div>  <a href="{"admin/content/edit-page/`$content.id`"|rA}">{"edit"|t}</a></div>
  <div> <a href="{"admin/inset-images/page"|rA}">{"inset_images"|t}</a></div>
  <div> <a href="{"admin/content/del-page/`$content.id`"|rA}" onclick="javascript:return confirm('{"content_page_delete"|t}?')">{"delete"|t}</a></div>
  <div><a href="{"admin/content"|rA}">{"show_all"|t}</a></div>

 <div class="clear"></div>
</div>
{/if}{* admin links *}

<h1>{$content.title}</h1>

{$content.text}

{if $content.images}
<div class="content_images">
<h4>{"content_images"|t}</h4>
</div>
<div class="image-listing">
{foreach from=$content.images item=foto name=fotos}
	<div class="img">

		<a href="{$baseUrl}/data/photos/{$foto.link}.jpg" title="{$foto.descr}" rel="lightbox[roadtrip]"><img src="{$baseUrl}/data/photos/{$foto.link}-thumb.jpg" alt="{$foto.title}" /></a><br />{$foto.descr}
	</div>
{/foreach}
</div>

{/if}


{include file=_bottom.tpl}

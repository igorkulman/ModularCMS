{if $access.admin}
            <div class="admin-menu">
              <h2>{"administration"|t}</h2>
              <ul class="admin-menu">
                <li>
                  <a href="{"admin/content"|rA}">{"contents"|t}</a>
                  <ul>
                    <li><a href="{"admin/content/add-page"|rA}">{"content_newpage"|t}</a></li>
                    <li><a href="{"admin/inset-images/page"|rA}">{"content_insetimages"|t}</a></li>
                    <li><a href="{"admin/files"|rA}">{"content_files"|t}</a></li>
                    <li><a href="{"admin/content/add-link"|rA}">{"content_newlink"|t}</a></li>
                  </ul>
                </li>
               <li>
                  <a href="{"admin/news"|rA}">{"news"|t}</a>
                  <ul>
                    <li><a href="{"admin/news/add"|rA}">{"news_new"|t}</a></li>
                  </ul>
                </li>
                <li>
                  <a href="{"admin/blocks"|rA}">{"blocks"|t}</a>
                  <ul>
                    <li><a href="{"admin/blocks/add"|rA}">{"blocks_new"|t}</a></li>
                  </ul>
                </li>
                 <li>
                  <a href="{"admin/gallery"|rA}">{"gallery"|t}</a>
                  <ul>
                    <li><a href="{"admin/gallery/addalbumform"|rA}">{"gallery_newalbum"|t}</a></li>
                    <li><a href="{"admin/gallery/addfotoform"|rA}">{"gallery_addphotos"|t}</a></li>
                  </ul>
                </li>
                 <li>
                  <a href="{"admin/catalogue"|rA}">{"cat_catalogue"|t}</a>
                  <ul>
                    <li><a href="{"admin/catalogue/templates"|rA}">{"cat_templates"|t}</a>
                    	<ul>
							<li><a href="{"admin/catalogue/add-template"|rA}">{"cat_templates_add"|t}</a></li>
                    	</ul>
                    </li>
                    <li><a href="{"admin/catalogue/add-product"|rA}">{"cat_newproduct"|t}</a></li>
                    <li><a href="{"admin/catalogue/add-category"|rA}">{"cat_newcategory"|t}</a></li>
                  </ul>
                </li>
                <li>
                  <a href="{"admin/editor"|rA}">{"editors"|t}</a>
                  <ul>
                    <li><a href="{"admin/editor/add"|rA}">{"editors_new"|t}</a></li>
                  </ul>
                </li>
				 <li><a href="{"admin/config"|rA}">{"configuration"|t}</a>
				 <ul><li>
                  <a href="{"admin/babel"|rA}">{"babel"|t}</a>

                </li></ul>
				 </li>
                <li><a href="{"user/password"|rA}">{"user_changepassword"|t}</a></li>
                <li><a href="{"user/logout"|rA}">{"user_logout"|t}</a></li>
              </ul>
            </div>



{elseif $access.editor}

<div class="admin-menu">
              <h2>{"administration"|t}</h2>
              <ul class="admin-menu">
                <li>
                  <a href="{"admin/content"|rA}">{"contents"|t}</a>
                  <ul>
                    <li><a href="{"admin/content/add-page"|rA}">{"content_newpage"|t}</a></li>
                    <li><a href="{"admin/inset-images/page"|rA}">{"content_insetimages"|t}</a></li>
                    <li><a href="{"admin/content/add-link"|rA}">{"content_newlink"|t}</a></li>
                  </ul>
                </li>


                <li>
                  <a href="{"admin/news"|rA}">{"news"|t}</a>
                  <ul>
                    <li><a href="{"admin/news/add"|rA}">{"news_new"|t}</a></li>
                  </ul>
                </li>

                <li>
                  <a href="{"admin/catalogue"|rA}">{"cat_catalogue"|t}</a>
                  <ul>
                    <li><a href="{"admin/catalogue/templates"|rA}">{"cat_templates"|t}</a>
                    	<ul>
							<a href="{"admin/catalogue/add-template"|rA}">{"cat_templates_add"|t}</a>
                    	</ul>
                    </li>
                    <li><a href="{"admin/catalogue/add-product"|rA}">{"cat_newproduct"|t}</a></li>
                    <li><a href="{"admin/catalogue/add-category"|rA}">{"cat_newcategory"|t}</a></li>
                  </ul>
                </li>


                <li><a href="{"user/password"|rA}">{"user_changepassword"|t}</a></li>
                <li><a href="{"user/logout"|rA}">{"user_logout"|t}</a></li>
              </ul>
            </div>
{/if}

{include file=_top.tpl}
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      if (!validatePresent(document.forms.f.login,  'login_i' )) errs += 1;
      if (!validatePresent(document.forms.f.passwd, 'passwd_i')) errs += 1;
      if (!validatePresent(document.forms.f.passwd2, 'passwd2_i')) errs += 1;

     if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>


<div class="cms-tabs">
{if $content}
  <div class="selected">  <a href="{if $arg.2=="add"}{"admin/editor/add"|rA}{else}{"admin/editor/edit/`$content.id`"|rA}{/if}">{"edit"|t}</a></div>
{/if}
 <div {if !$content}class="selected"{/if}><a href="{"admin/editor"|rA}">{"show_all"|t}</a></div>
 <div class="clear"></div>
</div>

<h1>
{if $content.add or $error}
  {"editors_new"|t}
{elseif $content}
  {"editors_one"|t}: <em>{$content.login}</em>
{else}
  {"editors"|t}
{/if}
</h1>

{if $content}

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}



<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()" enctype="multipart/form-data">

   <div class="input">
      <label>{"login"|t}:</label>
      <input name="login" value="{$content.login}"
       onchange="validatePresent(document.forms.f.title, 'login_i')" />
      <span id="login_i" class="info">{"forms_req"|t}</span>
    </div>

     <div class="input">
      <label>{"password"|t}:</label>
      <input name="passwd" value="" type="password"
       onchange="validatePresent(document.forms.f.passwd, 'passwd_i')" />
      <span id="passwd_i" class="info">{"forms_req"|t}</span>
    </div>

     <div class="input">
      <label>{"password2"|t}:</label>
      <input name="passwd2" value="" type="password"
       onchange="validatePresent(document.forms.f.passwd2, 'passwd_i')" />
      <span id="passwd2_i" class="info">{"forms_req"|t}</span>
    </div>



    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{else}

{* tasks / actions *}
<ul class="tasks">
  <li><a href="{"admin/editor/add"|rA}">{"editors_new"|t}</a></li>
</ul>

<br />

<table class="listing bottom-15">
  <thead>
    <tr>
      <td style="width: 70px;">ID</td>
      <td>{"login"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>

    </tr>
  </thead>
  <tbody>
{foreach from=$editors item=i}
    <tr class="{cycle values="odd,even"}">
{* date *}
      <td style="width: 70px;">{$i.id}</td>
{* title *}
      <td class="wrap">{$i.login}</td>
{* edit *}
      <td class="icon">
        <a href="{"admin/editor/edit/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
{* delete *}
      <td class="icon">
        <a href="{"admin/editor/del/`$i.id`"|rA}" onclick="javascript:return confirm('{"editors_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
      </td>

    </tr>
{/foreach}
  </tbody>
</table>

{/if}

{include file=_bottom.tpl}

{include file=_top.tpl}

{if $content}
<script type="text/javascript" src="{$lib}tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  tinyMCE.init({ldelim}
      elements : "text",

      {include file='tiny_mce.tpl'}

  {rdelim});

	{include file='file_manager.tpl'}


  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      if (!validatePresent(document.forms.f.title, 'title_i')) errs += 1;

      if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>
{/if}

{if $content.add}
<div class="cms-tabs">
  <div class="selected">  &nbsp;{"edit"|t}&nbsp;{*</a>*}</div>
 <div><a href="{"admin/blocks"|rA}">{"show_all"|t}</a></div>
 <div class="clear"></div>
</div>
{elseif $content}
  <div class="cms-tabs">
  <div class="selected" >  <a href="{"admin/blocks/edit/`$content.id`"|rA}">{"edit"|t}</a></div>
  <div> <a href="{"admin/blocks/del/`$content.id`"|rA}" onclick="javascript:return confirm('{"block_delete"|t}')">{"delete"|t}</a></div>
  <div><a href="{"admin/blocks"|rA}">{"show_all"|t}</a></div>

 <div class="clear"></div>
</div>
{else}
  <div class="cms-tabs">
  <div class="selected"><a href="{"admin/blocks"|rA}">{"show_all"|t}</a></div>

 <div class="clear"></div>
</div>

{/if}

<h1>
{if $content.add}
  {"blocks_new"|t}
{elseif $content}
  {"blocks_one"|t}: <em>{$content.title}</em>
{else}
 {"blocks"|t}
{/if}
</h1>

{if $content}

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}

<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()" enctype="multipart/form-data">
    <div class="input">
      <label>{t admin=true sk="Nadpis bloku" en="Title"}:</label>
      <input name="title" value="{$content.title}"
       onchange="validatePresent(document.forms.f.title, 'title_i')" />
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <label>{"style"|t}:</label>
      <select name="style">
{foreach from=$styles item=i}
        <option value="{$i}"{if $i == $content.style} selected="selected"{/if}>{$i}</option>
{/foreach}
      </select>
    </div>
    <div class="input">
      <label>{"text"|t}:</label>
      <span class="info">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <textarea name="text" id="text" cols="50" rows="25" class="wysiwyg">{$content.text}</textarea>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{else}

<ul class="tasks">
  <li><a href="{"admin/blocks/add"|rA}">{"blocks_new"|t}</a></li>
</ul>

<br />

<table class="listing bottom-15">
  <thead>
    <tr>
      <td>{"title"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
{foreach from=$blocks item=i name=blocks}
    <tr class="{cycle values="odd,even"}">
{* block title *}
      <td class="wrap">{$i.title}</td>
{* edit *}
      <td class="icon">
        <a href="{"admin/blocks/edit/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
{* delete *}
      <td class="icon">
        <a href="{"admin/blocks/del/`$i.id`"|rA}" onclick="javascript:return confirm('{"blocks_delete"|t}')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
      </td>
{* move up *}
      <td class="icon">
{if !$smarty.foreach.blocks.first}
        <a href="{"admin/blocks/up/`$i.id`"|rA}"><img src="{$gfx}icons/up.png" alt="{"move_up"|t}" title="{"move_up"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
{* move down *}
      <td class="icon">
{if !$smarty.foreach.blocks.last}
        <a href="{"admin/blocks/down/`$i.id`"|rA}"><img src="{$gfx}icons/down.png" alt="{"move_down"|t}" title="{"move_down"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
    </tr>
{/foreach}
  </tbody>
</table>

{/if}

{include file=_bottom.tpl}

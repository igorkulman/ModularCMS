{include file=_top.tpl}

{if $access.editor}
<div class="cms-tabs">
   <div class="selected"><a href="{"news"|rA}">{"news2"|t}</a></div>
   <div><a href="{"admin/news"|rA}">{"news_admin"|t}</a></div>
   <div>  <a href="{"admin/news/add"|rA}">{"add_news"|t}</a></div>
 <div class="clear"></div>
</div>

{/if}

<h1>{"news"|t}</h1>

{foreach from=$messages item=i}
<div class="news_message">


  <div class="title">
    <b>{$i.title}</b>({$i.d})
  </div>


  <div class="text1">{$i.text}

  {if $access.editor}
    <a href="{"admin/news/edit/`$i.id`"|ra}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}"  width="16" /></a>
    <a href="{"admin/news/del/`$i.id`/page/$page"|ra}" onclick="javascript:return confirm('{"news_delete"|t}?')"><img width="16" src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
{/if}
  </div>
</div>
<div class="clear"></div>

{/foreach}

<div class="clear hh10"></div>

{include file=pager.tpl}

{include file=_bottom.tpl}

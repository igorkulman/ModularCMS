  {literal}
  function fileBrowserCallBack(field_name, url, type, win) {
			var connector = "../../../file_manager.php";

			my_field = field_name;
			my_win = win;

			switch (type) {
				case "image":
					connector += "?type=img";
					break;
				case "media":
					connector += "?type=media";
					break;
				case "flash": //for older versions of tinymce
					connector += "?type=media";
					break;
				case "file":
					connector += "?type=files";
					break;
			}

			window.open(connector, "file_manager", "modal,width=450,height=600,scrollbars=1");
		}
{/literal}
{include file=_top.tpl}

<h1>{"cat_templates"|t}</h1>

<ul class="tasks">
  <li><a href="{"admin/catalogue/add-template"|rA}">{"cat_templates_add"|t}</a></li>
</ul>

<p>{"cat_templates_text"|t}</p>

<table class="listing bottom-15">
  <thead>
    <tr>
      <td>{"title"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
{foreach from=$templates item=i name=templates}
    <tr class="{cycle values="odd,even"}">
{* title *}
      <td class="wrap">
        <a href="{"admin/catalogue/add-product/`$i.id`"|rA}">{$i.title}</a>
      </td>
{* edit *}
      <td class="icon">
        <a href="{"admin/catalogue/edit-template/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
{* delete *}
      <td class="icon">
        <a href="{"admin/catalogue/del-template/`$i.id`"|rA}" onclick="javascript:return confirm('{"cat_templates_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
      </td>
{* move up *}
      <td class="icon">
{if !$smarty.foreach.templates.first}
        <a href="{"admin/catalogue/up-template/`$i.id`"|rA}"><img src="{$gfx}icons/up.png" alt="{"move_up"|t}" title="{"move_up"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
{* move down *}
      <td class="icon">
{if !$smarty.foreach.templates.last}
        <a href="{"admin/catalogue/down-template/`$i.id`"|rA}"><img src="{$gfx}icons/down.png" alt="{"move_down"|t}" title="{"move_down"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
    </tr>
{/foreach}
  </tbody>
</table>

{include file=_bottom.tpl}

{include file=_top.tpl}

<div class="cms-tabs">
{foreach from=$langs item=i}
  <div {if $i==$arg.2}class="selected"{/if}><a href="{"admin/babel/`$i`"|rA}">{$i}</a></div>
{/foreach}
 <div class="clear"></div>
</div>


<h1>{"babel"|t}</h1>

{if $saved}
	<p>{"configuration_saved"|t}.</p>
{/if}

<div class="my-form">
  <form action="" method="post" name="f">


	{foreach from=$data item=i key=k}

	<div class="input">
      <label>{$k}:</label>
      <input name="{$k}" value="{$i}">
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>

	{/foreach}

	<div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_change"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="lang" value="{$arg.2}" />


  </form>
</div>

{include file=_bottom.tpl}
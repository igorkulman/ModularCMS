{include file=_top.tpl}
<div class="cms-tabs">
  <div> <a href="{"admin/inset-images/page"|rA}">{"inset_images"|t}</a></div>
 <div class="selected"><a href="{"admin/content"|rA}">{"show_all"|t}</a></div>

 <div class="clear"></div>
</div>
<h1>{t admin=true sk="Obsah" en="Content"}</h1>

<ul class="tasks">
  <li><a href="{"admin/content/add-page"|rA}">{"content_newpage"|t}</a></li>
  <li><a href="{"admin/inset-images/page"|rA}">{"content_insetimages"|t}</a></li>
  <li><a href="{"admin/content/add-link"|rA}">{"content_newlink"|t}</a></li>
</ul>

<p>
{"content_lang1"|t}
</p>

{foreach from=$pages_positions item=m key=position}
<table class="listing bottom-15">
  <thead>
    <tr>
      <td>{t arr=$m}</td>
      <td style="width: 40px;">{"type"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
{cycle reset=true print=false advance=false values="odd,even"}
{foreach from=$pages.$position item=i name=foo}
    <tr class="{cycle values="odd,even"}">
{* page/link title *}
      <td class="wrap{if $i.default} strong{/if}">
        {section name=foo start=0 loop=$i.depth}- {/section}
{if $i.type == "page"}
        <a href="{"content/`$i.content_id`"|rA}">{$i.title}</a>
{elseif $i.type == "link"}
        <a href="{$i.link|rUrl}">{$i.title}</a>
{/if}
      </td>
{* type: page/link *}
      <td{if $i.default} class="strong"{/if} style="width: 40px;">
{if $i.type == "page"}
        {"page"|t}
{elseif $i.type == "link"}
        {"link"|t}
{/if}
      </td>
{* edit *}
      <td class="icon">
        <a href="{"admin/content/edit-`$i.type`/`$i.content_id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
{* delete *}
      <td class="icon">
{if $i.type == "page"}
        <a href="{"admin/content/del-page/`$i.content_id`"|rA}" onclick="javascript:return confirm('{"content_page_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
{elseif $i.type == "link"}
        <a href="{"admin/content/del-link/`$i.id`"|rA}" onclick="javascript:return confirm('{"content_link_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
{/if}
      </td>
{* as default page *}
      <td class="icon">
{if !$i.default && $i.type == "page"}
        <a href="{"admin/content/default-page/`$i.content_id`"|rA}"><img src="{$gfx}icons/flag.png" alt="{"set_title"|t}" title="{"set_title"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
{* move up *}
      <td class="icon">
{if !$smarty.foreach.foo.first}
        <a href="{"admin/content/up/`$i.id`"|rA}"><img src="{$gfx}icons/up.png" alt="{"move_up"|t}" title="{"move_up"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
{* move down *}
      <td class="icon">
{if !$smarty.foreach.foo.last}
        <a href="{"admin/content/down/`$i.id`"|rA}"><img src="{$gfx}icons/down.png" alt="{"move_down"|t}" title="{"move_down"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
    </tr>
{/foreach}
  </tbody>
</table>

{/foreach}

{include file=_bottom.tpl}

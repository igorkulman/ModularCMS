      mode : "textareas",

      language : "{$adminLang}",
      theme : "advanced",
      relative_urls : false,
      {*entities : "",*}
      document_base_url : "{$baseUrl}/",

      button_tile_map:          true,
      verify_html:              false,
      {*entity_encoding:          "named",*}
      accessibility_focus:      false,
      hide_selects_on_submit:   true,
      apply_source_formatting : true,
      cleanup_on_startup : false,
      file_browser_callback : "fileBrowserCallBack",

      {*entities : "160,nbsp,60,lt,62,gt",*}

      plugins:                  "style,advlink,paste,nonbreaking,searchreplace,advimage,table,contextmenu,safari",

      external_image_list_url : "{"admin/inset-images/page/list"|rA}",
      external_link_list_url:   "{"admin/content/link-list"|rA}",

      theme_advanced_toolbar_location : "top",
      theme_advanced_toolbar_align : "left",
      theme_advanced_blockformats:      "h3,h4,h5,h6,p,pre",

      theme_advanced_buttons1:  "bold,italic,underline,strikethrough,sub,sup,separator,justifyleft,justifycenter,justifyright,justifyfull,separator,bullist,numlist,outdent,indent,separator,forecolor,backcolor",
      theme_advanced_buttons2:  "pastetext,pasteword,selectall,separator,undo,redo,separator,search,replace,separator,link,unlink,anchor,separator,removeformat,cleanup,code,charmap,nonbreaking,separator,image",
      theme_advanced_buttons3:  "formatselect,separator,tablecontrols",


      extended_valid_elements : "font[face|size|color|style]"

{include file=_top.tpl}

{literal}
<script language="javascript" type="text/javascript">
<!--

  var lang = '{/literal}{$lang}{literal}';


  function validateForm() {
      var elem;
      var errs = 0;

      if (!validatePresent(document.forms.f.meno,  'meno_i' )) errs += 1;
      if (!validatePresent(document.forms.f.ulica, 'ulica_i')) errs += 1;
      if (!validatePresent(document.forms.f.psc, 'psc_i')) errs += 1;
      if (!validatePresent(document.forms.f.mesto, 'mesto_i')) errs += 1;
      if (!validatePresent(document.forms.f.telefon, 'telefon_i')) errs += 1;

      if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  }
  //-->
</script>
{/literal}

<h2>{"cat_form"|t}</h2>

<div class="my-form">
<form action="" name="f" method="post" onsubmit="return validateForm()">



    <div class="input">

      <label>{"name_firm"|t} </label>

      <input name="meno" value=""
       onchange="validatePresent(document.forms.f.meno, 'meno_i')" />
      <span id="meno_i" class="info">{if $lang=="sk"}Povinné{else}Required{/if}</span>
    </div>

    <div class="input">
      <label>{"contact_person"|t}</label>

      <input name="osoba" value="" />
      <span class="info"></span>
    </div>


    <div class="input">

      <label>{"street"|t}</label>

      <input name="ulica" value=""
       onchange="validatePresent(document.forms.f.ulica, 'ulica_i')" />
      <span id="ulica_i" class="info">{if $lang=="sk"}Povinné{else}Required{/if}</span>

    </div>

    <div class="input">

      <label>{"zip"|t}</label>

      <input name="psc" value=""
       onchange="validatePresent(document.forms.f.psc, 'psc_i')" />
      <span id="psc_i" class="info">{if $lang=="sk"}Povinné{else}Required{/if}</span>
    </div>

    <div class="input">


      <label>{"city"|t}</label>

      <input name="mesto" value=""
       onchange="validatePresent(document.forms.f.mesto, 'mesto_i')" />
      <span id="mesto_i" class="info">{if $lang=="sk"}Povinné{else}Required{/if}</span>
    </div>




    <div class="input">

      <label>{"phone"|t}</label>


      <input name="telefon" value=""
       onchange="validatePresent(document.forms.f.telefon, 'telefon_i')" />
      <span id="telefon_i" class="info">{if $lang=="sk"}Povinné{else}Required{/if}</span>
    </div>

    <div class="input">

      <label>{"fax"|t}</label>

      <input name="fax" value=""  />
      <span class="info"></span>
    </div>


    <div class="input">

      <label>{"email"|t}</label>

      <input name="email" value=""  />
      <span class="info"></span>
    </div>


    <div class="input">

	    <label>&nbsp;</label>
    	<input type="submit" value="{"continue"|t}" />
    </div>

    <input type="hidden" value="ok" name="sent" />

</div>

</form>

{include file=_bottom.tpl}
{include file=_top.tpl}

{if $arg[1] == "password"}
<script type="text/javascript">
<!--
  lang = '{$adminLang}';

  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      if (!validateStr(document.forms.f.pwd2, 'pwd2_i', true, 6, 20)) errs += 1;
      if (!validateStr(document.forms.f.pwd1, 'pwd1_i', true, 6, 20)) errs += 1;

      if (errs >= 1) {ldelim}
          alert('{"user_e_p1"|t}!');
      {rdelim} else if (document.forms.f.pwd1.value != document.forms.f.pwd2.value) {ldelim}
          alert('{"user_e_p2"|t}!');
          errs += 1;
      {rdelim}

      return (errs == 0);
  {rdelim}
//-->
</script>
{/if}

<div class="gradient">
{if $arg[1] == "password"}
  <h1>{"user_changepassword"|t}</h1>
{else}
  <h1>{"user_login2"|t}</h1>
{/if}
</div>

{if $error}
<div class="error">{t arr=$error}</div>
{/if}

{if $arg[1] == "password"}
<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()">
    <div class="input">
      <label>{"user_old_passwd"|t}:</label>
      <input type="password" name="password"  value="" />
    </div>
    <div class="input">
      <label>{"user_new_passwd"|t}:</label>
      <input type="password" name="pwd1" maxlength="20"
       onchange="validateStr(document.forms.f.pwd1, 'pwd1_i', true, 6, 20)" />
      <span id="pwd1_i" class="info">6-20 {"chars"|t}</span>
    </div>
    <div class="input">
      <label>{"user_new_passwd"|t}:</label>
      <input type="password" name="pwd2" maxlength="20"
       onchange="validateStr(document.forms.f.pwd2, 'pwd2_i', true, 6, 20)" />
      <span id="pwd1_i" class="info">6-20 {"chars"|t}</span>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_change"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="change" />
  </form>
</div>

{else}{* login form *}

<div class="my-form">
  <form action="" method="post" name="f">
    <div class="input">
      <label>{"user_login"|t}:</label>
      <input type="text" name="login" value="" />
    </div>
    <div class="input">
      <label>{"user_passwd"|t}:</label>
      <input type="password" name="password"  value="" />
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"user_login"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="login" />
  </form>
</div>
{/if}

{include file=_bottom.tpl}

{include file=_top.tpl}

{if $access.admin}
<div class="cms-tabs">
{if $action=="album"}
  <div> <a href="{"admin/gallery/albumphotos/`$data.id`"|rA}">{"edit_photos"|t}</a></div>
  <div> <a href="{"admin/gallery/changealbumform/`$data.id`"|rA}">{"edit"|t}</a></div>
  <div> <a href="{"admin/gallery/addfotoform/`$data.id`"|rA}">{"add_photos2"|t}</a></div>
{elseif $action=="foto"}
{else}
  <div> <a href="{"admin/gallery"|rA}">{"gallery3"|t}</a> </div>
  <div> <a href="{"admin/gallery/addalbumform"|rA}">{"new_album"|t}</a> </div>
{/if}
<div class="clear"></div>
</div>
{/if}


{if $action=="album"}
<h2>{$data.title}</h2>
{if $data.text}<p>{$data.text}</p>{/if}
<div class="image-listing">
{foreach from=$fotos item=foto name=fotos}
	<div class="img">

		<a href="{$baseUrl}/data/photos/{$foto.link}.jpg" title="{$foto.descr}" rel="lightbox[roadtrip]"><img src="{$baseUrl}/data/photos/{$foto.link}-thumb.jpg" alt="{$foto.title}" /></a><br />{$foto.descr}
	</div>
{/foreach}
</div>
<div class="clear hh15"></div>
{include file=pager.tpl}


{elseif $action=="foto"}
<h2>{$album}</h2>
{include file=pager.tpl}
<div class="clear hh15"></div>
<div align="center">
	<img src="{$baseUrl}/{$photo_path}/{$foto.link}.jpg" alt="" style="border: 3px solid #bababa;"/>
</div>
<div class="clear hh15"></div>
{include file=pager.tpl}

{else}
<h2>{"gallery"|t}</h2>

<div class="image-listing">
{foreach from=$albums item=album name=albumy}
	<div class="img">
		<a href="{"gallery/`$album.id`"|rA}"><img src="{$baseUrl}/data/photos/{$album.foto}-thumb.jpg" alt="{$album.title}" /></a>
		<br /><a href="{"gallery/`$album.id`"|rA}">{$album.title}</a>
	</div>
{/foreach}
</div>
<div class="clear hh15"></div>
{include file=pager.tpl}
{/if}


{include file=_bottom.tpl}

{if $prev || $next}
{if !$pagerLang}
{assign var="pagerLang" value=$lang}
{/if}
<div class="pager">
{if $prev}
  <a href="{"$pager/$first"|rA}" class="pager">&laquo;&laquo;&nbsp;{"first"|t}</a>
  <a href="{"$pager/$prev"|rA}" class="pager">&laquo;&nbsp;{"prev"|t}</a>
{else}
  <span class="disabled">&laquo;&laquo;&nbsp;{"first"|t}</span>
  <span class="disabled">&laquo;&nbsp;{"prev"|t}</span>
{/if}
  <span class="info">[ {$page} / {$pages} ]</span>
{if $next}
  <a href="{"$pager/$next"|rA}" class="pager">{"next"|t}&nbsp;&raquo;</a>
  <a href="{"$pager/$last"|rA}" class="pager">{"last"|t}&nbsp;&raquo;&raquo;</a>
{else}
  <span class="disabled">{"next"|t}&nbsp;&raquo;</span>
  <span class="disabled">{"last"|t}&nbsp;&raquo;&raquo;</span>
{/if}
</div>
{/if}

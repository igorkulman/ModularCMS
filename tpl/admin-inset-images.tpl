{include file=_top.tpl}
<div class="cms-tabs">
  <div class="selected"> <a href="{"admin/inset-images/page"|rA}">{"inset_images"|t}</a></div>

 <div class="clear"></div>
</div>

<h1>{if $title}{"content_insetimages"|t} <em>{$title.sk}</em>{else}{"content_insetimages"|t}{/if}</h1>

{*
{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}
*}

{if !$title}
<p>
{"content_inset_text"|t}
</p>
{/if}

<p>{"gallery_warning1"|t} {#inset_images_size#}px ({"gallery_warning2"|t}).</p>

{include file=pager.tpl pagerLang=$adminLang}

<div class="image-listing">
{foreach from=$images item=i key=id}
  <div class="img">
    <a href="{$dir}/{$i}.jpg" rel="lightbox[roadtrip]"><img src="{$dir}/{$i}-thumb.jpg" alt="" /></a>
    <div class="actions">
      {$i}<br />
      <a href="{"admin/inset-images/$pageId/$page/del/$id"|rA}" onclick="javascript:return confirm('{t admin=true sk="Naozaj zmazať túto fotografiu?" en="Do you really want to delete this image?"}')">{t admin=true sk="zmazať" en="delete"}</a>
    </div>
  </div>
{/foreach}
</div>
<div class="clear"></div>

{include file=pager.tpl pagerLang=$adminLang}

<div class="clear hh5"></div>
<div class="my-form">
  <h2>{"content_inset_add"|t}</h2>
  <form action="" method="post" enctype="multipart/form-data">
{section name=input start=1 loop=$input}
    <div class="input">
      <label>{"image"|t} {$smarty.section.input.index}:</label>
      <input type="file" name="image[]" class="file" accept="image/jpeg" />
    </div>
{/section}
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_add"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="add" />
  </form>
</div>


{include file=_bottom.tpl}

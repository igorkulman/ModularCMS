{include file=_top.tpl}

<h2>{"cat_cart"|t}</h2>

{if $new}
	<p>{"cat_cart_added"|t}</p>
{/if}

<h3>{"cat_cart_content"|t}</h3>

<table class="listing bottom-15">
  <tbody>
{cycle reset=true print=false advance=false values="even,odd"}
{foreach from=$data item=i key=k}
    <tr class="{cycle values="even,odd"}">

      <td>
      	{$i.pocet}
      </td>

      <td>
      	{$i.nazov}
      </td>

       <td>
      	{$i.cena}
      </td>

      <td>
      	<a href="{"catalogue/del/`$i.id`"|rA}">{"cat_cart_del"|t}</a>
      </td>

   </tr>
{/foreach}
   </tbody>
 </table>

 <p>{"cart_sum"|t}: {$spolu} {$currency}</p>

 {if !$data}
 	<p>{"cat_cart_empty"|t}.<p>
 {/if}


{if $ref}<p><a href="{$ref}">{"back"|t}</p>{/if}
{if $data}
<p>
	<a href="{"catalogue/cart/form"|rA}">{"cat_cart_order"|t}</a>
	<a href="{"catalogue/cart/empty"|rA}">{"cat_cart_empty"|t}</a>
</p>
{/if}

{include file=_bottom.tpl}
{include file=_top.tpl}

<h1>{"cat_catalogue"|t}</h1>

<ul class="tasks">
  <li><a href="{"admin/catalogue/add-product"|rA}">{"cat_newproduct"|t}</a></li>
  <li><a href="{"admin/catalogue/add-category"|rA}">{"cat_newcategory"|t}</a></li>
</ul>



<table class="listing bottom-15">
  <thead>
    <tr>
      <td>{"title"|t}</td>
      <td style="width: 40px;">{"cat_count"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
{foreach from=$categories item=i name=bar}
    <tr class="{cycle values="odd,even"}">
      <td class="wrap">
        {section name=foo start=0 loop=$i.depth}- {/section}
        <a href="{"catalogue/`$i.id`"|rA}">{$i.title}</a>
      </td>
      <td class="right" style="width: 40px;">{$i.products|number}</td>
      <td class="icon">
        <a href="{"admin/catalogue/edit-category/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
      <td class="icon">
        <a href="{"admin/catalogue/del-category/`$i.id`"|rA}" onclick="javascript:return confirm('{"cat_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
      </td>
      <td class="icon">
{if !$smarty.foreach.bar.first}
        <a href="{"admin/catalogue/up-category/`$i.id`"|rA}"><img src="{$gfx}icons/up.png" alt="{"move_up"|t}" title="{"move_up"}" /></a>
{else}
        &nbsp;
{/if}
      </td>
      <td class="icon">
{if !$smarty.foreach.bar.last}
        <a href="{"admin/catalogue/down-category/`$i.id`"|rA}"><img src="{$gfx}icons/down.png" alt="{"move_down"|t}" title="{"move_down"|t}" /></a>
{else}
        &nbsp;
{/if}
      </td>
    </tr>
{/foreach}
  </tbody>
</table>

{if $products}
<h2>{"cat_nocat"|t}</h2>

<p>
{"cat_nocat_text"|t}
</p>

<table class="listing bottom-15">
  <thead>
    <tr>
      <td>{"title"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>
    </tr>
  </thead>
  <tbody>
{cycle reset=true print=false advance=false values="odd,even"}
{foreach from=$products item=i}
    <tr class="{cycle values="odd,even"}">
      <td class="wrap">
        <a href="{"catalogue/product/`$i.id`"|rA}">{$i.title}</a>
      </td>
      <td class="icon">
        <a href="{"admin/catalogue/edit-product/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
      <td class="icon">
        <a href="{"admin/catalogue/del-product/`$i.id`"|rA}" onclick="javascript:return confirm('{"cat_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"cat_delete"|t}" title="{"cat_delete"|t}" /></a>
      </td>
    </tr>
{/foreach}
  </tbody>
</table>
{/if}


{include file=_bottom.tpl}

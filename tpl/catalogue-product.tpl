{include file=_top.tpl}

{if $access.editor}{* admin links *}
<div class="cms-tabs">
  <div> <a href="{"admin/catalogue/edit-product/`$product.id`"|rA}">{"edit"|t}</a></div>
  <div> <a href="{"admin/catalogue/del-product/`$product.id`"|rA}" onclick="javascript:return confirm('{"cat_product_delete"|t}?')">{"delete"|t}</a></div>
  <div class="selected"> <a href="{"catalogue/product/`$content.id`"|rA}">{"show"|t}</a></div>
  <div class="clear"></div>
</div>
{/if}{* admin links *}

<h2>{$product.title}</h2>

<div class="product-info">
{if $product.image}
  <div class="product-default-image">
    <a href="{"catalogue/product/`$product.id`/image/`$product.default_image`"|rA}"><img src="{$product.image}" alt="{$product.title}" /></a>
{if $access.editor}
    <div class="actions">
      <a href="{"catalogue/product/`$product.id`/unset-default-image"|rA}">{"unset"|t}</a> |
      <a href="{"catalogue/product/`$product.id`/del-image/`$product.default_image`"|rA}" onclick="javascript:return confirm('{"delete_file"|t}?')">{"delete"|t}</a>
    </div>
{/if}
  </div>
{/if}
  {$product.text}
  <div class="clear"></div>
  <p>{"price"|t}: {$product.price} {$product.currency}</p>
</div>

<p>
{foreach from=$product.details item=i name=foo}
  {$i.title|default:"&nbsp;"}: {$i.value} {$i.unit}
{if $access.editor}
    {if !$smarty.foreach.foo.first}<a href="{"admin/catalogue/up-item/`$product.id`/`$i.id`"|rA}"><img src="{$gfx}icons/up.png" alt="{"move_up"|t}" title="{"move_up"|t}" /></a>{/if}
   {if !$smarty.foreach.foo.last} <a href="{"admin/catalogue/down-item/`$product.id`/`$i.id`"|rA}"><img src="{$gfx}icons/down.png" alt="{"move_down"|t}" title="{"move_down"|t}" /></a>{/if}
{/if}
<br />
</p>
{/foreach}

{if #catalogue_shop#==1}
   <div class="product-cart">

        <form name="f" action="" method="post">
          <input type="hidden" name="id" value="{$i.id}">


			{"cat_cart_add"|t}
            <input type="text" name="pocet" id="pocet" value="1" />
			<input type="submit" name="submit" value="{"car_cart_addbtn"|t}" />
        </form>
   </div>
{/if}

<div class="clear hh15"></div>

{if $images}
<div class="image-listing">
{foreach from=$images item=i name=foo}
  <div class="img">
    <a rel="lightbox[roadtrip]" href="{$i.image_full}"><img src="{$i.image}" alt="" /></a>
{if $access.editor}
    <div class="actions">
      <a href="{"catalogue/product/`$product.id`/default-image/`$i.id`"|rA}">{"default"|t}</a> |
      <a href="{"catalogue/product/`$product.id`/del-image/`$i.id`"|rA}" onclick="javascript:return confirm('{"delete_file"|t}?')">{"delete"|t}</a>
      <br />
      {if !$smarty.foreach.foo.first}<a href="{"catalogue/product/`$product.id`/up-image/`$i.id`"|rA}">|&laquo;&laquo;</a>{/if}
      {if !$smarty.foreach.foo.last}<a href="{"catalogue/product/`$product.id`/down-image/`$i.id`"|rA}">&raquo;&raquo;|</a>{/if}
    </div>
{/if}
  </div>
{/foreach}
</div>
<div class="clear hh15"></div>
{/if}



{if $access.editor}
<div class="my-form">
  <h2>{"cat_add_images"|t}</h2>
  <p>{"gallery_warning1"|t} {#catalogue_size#} ({"gallery_warning2"|t}).</p>
  <form action="" method="post" enctype="multipart/form-data">
{section name=input start=1 loop=3}
    <div class="input">
      <label>{"image"|t} {$smarty.section.input.index}:</label>
      <input type="file" name="image[]" class="file" accept="image/jpeg" />
    </div>
{/section}
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_add"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="add" />
  </form>
</div>
{/if}



{include file=_bottom.tpl}

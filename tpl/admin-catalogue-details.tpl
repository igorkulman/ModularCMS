{include file=_top.tpl}

{if $categories or !$content.add}

<script language="javascript" type="text/javascript" src="{$lib}tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  tinyMCE.init({ldelim}
      elements : "text",

      {include file='tiny_mce.tpl'}

	,mode : "textareas"

  {rdelim});

  {include file='file_manager.tpl'}



  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      //if (!validatePresent(document.forms.f.menu,  'menu_i' )) errs += 1;
      {if $arg.2=="add-product" or $arg.2=="add-template"}
	      {foreach from=$languages item=i key=k}
		      if (!validatePresent(document.forms.f.title_{$k}, 'title_{$k}_i')) errs += 1;
	      {/foreach}
	  {else}
	  		if (!validatePresent(document.forms.f.title, 'title_i')) errs += 1;
	  {/if}

       if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>

{if $access.editor}{* admin links *}
<div class="cms-tabs">
  <div class="selected"> <a href="{"admin/catalogue/edit-product/`$content.id`"|rA}">{"edit"|t}</a></div>
  <div> <a href="{"admin/catalogue/del-product/`$content.id`"|rA}" onclick="javascript:return confirm('{"cat_product_delete"|t}?')">{"delete"|t}</a></div>
  <div> <a href="{"catalogue/product/`$content.id`"|rA}">{"show"|t}</a></div>
  <div class="clear"></div>
</div>
{/if}{* admin links *}

<h1>
{if $template}
{if $content.add}
  {"cat_templates_add"|t}
{else}
  {"cat_templates"|t}: <em>{$content.title}</em>
{/if}
{elseif $product}
{if $content.add}
  {"cat_newproduct"|t}{if $content.template}: <em>{$content.template}</em>{/if}
{else}
  {"cart_products"|t}: <em>{$content.title}</em>
{/if}
{/if}
</h1>

{if $content.add}
<ul>

{if $template}
  <li>{"cat_templates_t2"|t}.</li>
{elseif $product}
  <li>{"cat_products_t2"|t}.</li>
{/if}

</ul>
{/if}

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}



<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()">

{if $arg.2=="add-product" or $arg.2=="add-template"}

	{foreach from=$languages item=i key=k}
<div class="input">
      <label>{"title"|t} ({$k}):</label>
      <input name="title_{$k}" value="{$content.title}"
       onchange="validatePresent(document.forms.f.title_{$k}, 'title_{$k}_i')" />
      <span id="title_{$k}_i" class="info">{"forms_req"|t}</span>
    </div>

	{/foreach}

{else}
	<div class="input">
      <label>{"title"|t}:</label>
      <input name="title" value="{$content.title}"
       onchange="validatePresent(document.forms.f.title, 'title_i')" />
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>

{/if}
    <div class="input">
      <label>{"cat_category"|t}:</label>
      <select name="category_id">
{foreach from=$categories item=i}
        <option value="{$i.id}"{if $i.id == $content.category_id} selected="selected"{/if}>{section name=indent start=0 loop=$i.depth} -{/section} {t arr=$i.title}</option>
{/foreach}
      </select>
    </div>


{if $template}
    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
        {"cat_templates_t4"|t}.
      </div>
    </div>
{/if}

{if #catalogue_shop#==1}
     <div class="input">
      <label>{"price"|t}:</label>
      <input name="price" value="{$content.price}" />
      <span id="price_i" class="info">{"forms_opt"|t}</span>
    </div>
      <div class="input">
      <label>{"currency"|t}:</label>
      <input name="currency" value="{$content.currency}" />
      <span id="currency_i" class="info">{"forms_opt"|t}</span>
    </div>
{/if}

{if $arg.2=="add-product" or $arg.2=="add-template"}
{foreach from=$languages item=i key=k}
<div class="input">
      <label>{"cat_desc"|t} ({$k}):</label>
    </div>
    <div class="input" style="clear:both">
      <textarea name="text_{$k}" id="text_{$k}" cols="50" rows="13" class="wysiwyg">{$content.text}</textarea>
    </div>
    <br />
    {/foreach}
{else}
    <div class="input">
      <label>{"cat_desc"|t}:</label>
    </div>
    <div class="input" style="clear:both">
      <textarea name="text" id="text" cols="50" rows="13" class="wysiwyg">{$content.text}</textarea>
    </div>
    <br />
{/if}
{foreach from=$details item=i}
    <h2>{"cat_item"|t} {$i.number}</h2>
{foreach from=$languages item=language key=l}
    <div class="input">
      <label>{"title"|t} ({$language.name}):</label>
      <input name="detail_title[{$l}][]" value="{$i.title.$l}" />
    </div>
{/foreach}
    <div class="input">
      <label>{"cat_value"|t}:</label>
      <input name="detail_value[]" value="{$i.value}" />
    </div>
    <div class="input">
      <label>{"cat_unit"|t}:</label>
      <input name="detail_unit[]" value="{$i.unit}" />
    </div>
{/foreach}
{*
    <h2>{"cat_send_form"|t}</h2>
    <div class="input">
      <label>&nbsp;</label>
      <input type="checkbox" name="more-items" value="yes" class="checkbox" />
      {"cat_moreitems"|t}
    </div>*}
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{else}

<p>{"catalogue_nocats"|t}.</p>

{/if}

{include file=_bottom.tpl}

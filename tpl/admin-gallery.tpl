{include file=_top.tpl}

<script type="text/javascript" src="{$lib}tiny_mce/tiny_mce.js"></script
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  tinyMCE.init({ldelim}
  elements : "{foreach from=$languages item=i key=k name=foo}text_{$k}{if !$smarty.foreach.foo.last},{/if}{/foreach}",

  {include file='tiny_mce.tpl'}

  {rdelim});



  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      //if (!validatePresent(document.forms.f.menu,  'menu_i' )) errs += 1;
      {foreach from=$languages item=i key=k} if (!validatePresent(document.forms.f.title_{$k}, 'title_{$k}_i')) errs += 1; {/foreach}

      if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>

<div class="cms-tabs">
  <div {if $action=="addalbumform"}class="selected"{/if}>  <a href="{"admin/gallery/addalbumform"|rA}">{"new_album"|t}</a></div>
   <div {if $action=="addfotoform"}class="selected"{/if}>  <a href="{"admin/gallery/addfotoform"|rA}">{"add_photos"|t}</a></div>
   <div {if $arg.2==""}class="selected"{/if}>  <a href="{"admin/gallery"|rA}">{"gallery2"|t}</a></div>
 <div class="clear"></div>
</div>

{if $action=="addalbumform"}
<h1>{"gallery_newalbum"|t}</h1>
<div class="my-form">
<form action="" method="POST" enctype="multipart/form-data" name="f" onsubmit="return validateForm()">

{foreach from=$languages item=i key=k}
	<div class="input">
		<label>{"title"|t} ({$k}):</label>
		<input name="title_{$k}" id="title_{$k}" value="{$content.title}"
       onchange="validatePresent($('title_{$k}'), 'title_{$k}_i');"/>
      <span id="title_{$k}_i" class="info">{"forms_req"|t}</span>
	</div>

<h3>{"description"|t} ({$k})</h3>
	<div class="input">
		<textarea name="text_{$k}" id="text_{$k}" cols="50" rows="25" class="wysiwyg"></textarea>
	</div>

{/foreach}

	<div class="input">
		<label>&nbsp;</label>
		<input type="submit" value="{"btn_add"|t} &raquo;" class="submit" />
	</div>
	<input type="hidden" name="action" value="addalbum" />
</form>
</div>


{elseif $action=="addfotoform"}
<h1>{"gallery_addphotos"|t}</h1>


{if !$albums}<b>{"gallery_noalbum"|t}</b>
{else}

<p>{"gallery_warning1"|t} {#gallery_images_size#}px ({"gallery_warning2"|t}).</p>
<div class="clear hh5"></div>
<div class="my-form">



  <form action="" method="post" enctype="multipart/form-data">
   <input type="hidden" name="album" value="{$arg.3}">
    <div class="input">
	  <label>{"gallery_toalbum"|t} :</label>
	  <select name="album">
	  	{foreach from=$albums item=i}
	  		<option value="{$i.id}" {if $arg.3==$i.id}selected{/if}>{$i.title}</option>
	  	{/foreach}
	  </select>
  </div>


{section name=input start=1 loop=6}

      <hr /><br />
    <div class="input">
      <label>{"image"|t} :</label>
      <input type="file" name="image[]" class="file" accept="image/jpeg" />
    </div>

	{foreach from=$langs item=i key=k}
	    <div class="input">
	      <label>{"title"|t} ({$i}) :</label>
	      <input type="text" name="title[{$k}][]" />
	    </div>

	   {* <div class="input">
	   <label>{"description"|t} ({$i}) :</label>
	     <textarea name="desc[{$k}][]" cols="15" rows="3" style="width:400px; margin-bottom:10px;"></textarea>
	    </div>*}
	{/foreach}


{/section}

    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_add"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="addfoto" />
  </form>
</div>

{/if}

{elseif $action=="editfotoform"}

<div class="clear hh5"></div>
<div class="my-form">
  <form action="" method="post" enctype="multipart/form-data">

    <img src="{$baseUrl}/data/photos/{$foto.link}.jpg" alt="" style="border: 3px solid #bababa;"/>
    <br /><br />
     <div class="input">
     <label>{"title"|t} :</label>
     <input type="text" name="title" value="{$foto.title}" />
    </div>

{*  <div class="input">
     <label>{"description"|t} :</label><br />
     <textarea name="desc" class="wysiwyg" cols="50" rows="15">{$foto.descr}</textarea>
    </div>*}


    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_change"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="editfoto" />
    <input type="hidden" name="id" value="{$foto.id}" />
  </form>
</div>

{elseif $action=="changealbumform"}
<h1>{"album"|t}: <em>{$album.title}</em></h1>
<div class="my-form">
<form action="" method="POST" enctype="multipart/form-data" name="f" onsubmit="return validateForm()">
	<div class="input">
		<label>{"title"|t} :</label>
		<input name="title" id="title" value="{$album.title}"
       onchange="validatePresent($('title'), 'title_i');/>
      <span id="title_i" class="info">{"forms_req"|t}</span>
	</div>

	<div class="input">
		<textarea name="text" id="text" cols="50" rows="25" class="wysiwyg">{$album.text}</textarea>
	</div>

	<div class="input">
		<label>&nbsp;</label>
		<input type="submit" value="{"btn_change"|t}" class="submit" />
	</div>
	<input type="hidden" name="action" value="changealbum" />
</form>
</div>


{elseif $action=="albumfotos"}
<h1>{"gallery"|t} - {$album_title}</h1>


  {foreach from=$AdminGal_fotos item=fotka name=fotky}
    <div class="fotoitem">
	     <div class="foto"><a href="{"admin/gallery/edit/`$fotka.id`"|rA}"><img src="{$baseUrl}/data/photos/{$fotka.link}-thumb.jpg" /></a></div>
	     {$fotka.title}<br />
	      <a href="{"admin/gallery/top/`$fotka.id`"|rA}" title="vlavo" >&lt;</a>  <a href="{"admin/gallery/del/`$fotka.id`"|rA}" >zmazať</a> <a href="{"admin/gallery/down/`$fotka.id`"|rA}" title="vpravo" >&gt;</a>
	  </div>
	{foreachelse}
	  <p>{"gallery_none"|t}</p>
	{/foreach}


	<div class="clear">&nbsp;</div>

{else}
{if $error}<h1>{$error}</h1><br /><br />{/if}

<h1>{"gallery"|t}</h1>
<ul class="tasks">
  <li><a href="{"admin/gallery/addalbumform"|rA}">{"gallery_newalbum"|t}</a></li>
  <li><a href="{"admin/gallery/addfotoform"|rA}">{"gallery_addphotos"|t}</a></li>
</ul>

<br />

<table class="listing bottom-15">
	<thead>
		<tr>
			<td>{"album"|t}</td>
      		<td class="icon">&nbsp;</td>
      		<td class="icon">&nbsp;</td>
      		<td class="icon">&nbsp;</td>
      		<td class="icon">&nbsp;</td>
		</tr>
	</thead>
{foreach from=$AdminGal_albums item=album}
	<tr class="{cycle values="odd,even"}">
		<td class="wrap"><a href="{"admin/gallery/albumphotos/`$album.id`"|rA}" title="{$album.title}">{$album.title}</a></td>
		<td class="icon"><a href="{"admin/gallery/changealbumform/`$album.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a></td>
		<td class="icon"><a href="{"admin/gallery/deletealbum/`$album.id`"|rA}" onclick="javascript:return confirm('{"gallery_delete_album"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a></td>
		<td class="icon"><a href="{"admin/gallery/addfotoform/`$album.id`"|rA}"><img src="{$gfx}icons/add.png" alt="{"gallery_add"|t}" title="{"gallery_add"|t}" /></a></td>
	{if $album.visible=="t"}
			<td class="icon"><a href="{"admin/gallery/visible/false/`$album.id`"|rA}"><img src="{$gfx}icons/hide.png" alt="{"gallery_hide"|t}" title="{"gallery_hide"|t}" /></a></td>
		{else}
			<td class="icon"><a href="{"admin/gallery/visible/true/`$album.id`"|rA}"><img src="{$gfx}icons/visible.png" alt="{"gallery_visible"|t}" title="{"gallery_visible"|t}" /></a></td>
		{/if}
	</tr>
{/foreach}
</table>

{/if}



{include file=_bottom.tpl}

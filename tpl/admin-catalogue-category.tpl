{include file=_top.tpl}

<script type="text/javascript" src="{$lib}tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  tinyMCE.init({ldelim}
      elements :
      {if $arg.2 == "add-category"}
      "{foreach from=$languages item=language key=l name=foo}text_{$l}{if !$smarty.foreach.foo.last},{/if}{/foreach}",
      {else}
      "text",
      {/if}


      {include file='tiny_mce.tpl'}

  {rdelim});

  {include file='file_manager.tpl'}



  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      //if (!validatePresent(document.forms.f.menu,  'menu_i' )) errs += 1;
      {if $arg.2 == "add-category"}
      {foreach from=$languages item=language key=l}if (!validatePresent(document.forms.f.title_{$l}, 'title_{$l}_i')) errs += 1;{/foreach}
      {else}
      if (!validatePresent(document.forms.f.title, 'title_i')) errs += 1;
      {/if}

      if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>


<h1>
{if $content.add}
  {"cat_newcategory"|t}
{else}
  {"cat_category"|t}: <em>{$content.title}</em>
{/if}
</h1>

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}

<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()">

{if $arg.2 == "add-category"}
{foreach from=$languages item=language key=l}
	 <div class="input">
      <label>{"title"|t} ({$l}):</label>
      <input name="title_{$l}" value="{$content.title}"
       onchange="validatePresent(document.forms.f.title_{$l}, 'title_{$l}_i')" />
      <span id="title_{$l}_i" class="info">{"forms_req"|t}</span>
    </div>
{/foreach}
{else}
    <div class="input">
      <label>{"title"|t}:</label>
      <input name="title" value="{$content.title}"
       onchange="validatePresent(document.forms.f.title, 'title_i')" />
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>
{/if}

    <div class="input">
      <label>{"cat_parent"|t}:</label>
      <select name="parent_id">
{foreach from=$categories item=i}
        <option value="{$i.id}"{if $i.id == $content.parent_id} selected="selected"{/if}>{section name=indent start=0 loop=$i.depth} -{/section} {t arr=$i.title}</option>
{/foreach}
      </select>
    </div>

{if $arg.2 == "add-category"}
{foreach from=$languages item=language key=l}
    <div class="input">
      <label>{"cat_desc"|t} ({$l}):</label>
      <span class="info_nreq">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <textarea name="text_{$l}" id="text_{$l}" cols="50" rows="13" class="wysiwyg">{$content.text}</textarea>
    </div>
{/foreach}
{else}
    <div class="input">
      <label>{"cat_desc"|t}:</label>
      <span class="info_nreq">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <textarea name="text" id="text" cols="50" rows="13" class="wysiwyg">{$content.text}</textarea>
    </div>
{/if}
    <div class="input">
      <label>&nbsp;</label>
       <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{include file=_bottom.tpl}

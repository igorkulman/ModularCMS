<!-- content end -->

</div>
		
<div class="right">
		
          
            {include file=admin-links.tpl}
    	
		
<h2>{"navigation"|t}</h2>

{$menu.side}

<div class="right-news">		
<h2>{"news"|t}</h2>
{foreach from=$news item=i}
    <p><b>{$i.title}</b> ({$i.d}) <br />{$i.text}</p>
{/foreach}  
<p><a href="{"news"|rA}">... {"news_alllink"|t}</a></p>
</div>

{foreach from=$blocks item=i}
    <h2>{$i.title}</h2><p>{$i.text}</p>
{/foreach}  

</div>

<div id="clear"></div>

</div>

<div id="bottom">
</div>

</div>

<div id="footer">
Design by <a href="http://www.minimalistic-design.net">Minimalistic Design</a>. Copyright <a href="http://www.kulman.sk">Igor Kulman</a>. 
</div>

</body>
</html>

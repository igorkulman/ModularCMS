{include file=_top.tpl}

{if $access.editor}{* admin links *}
<div class="cms-tabs">
  <div><a href="{"admin/catalogue/edit-category/`$category.id`"|rA}">{"edit"|t}</a></div>
  <div><a href="{"admin/catalogue/del-category/`$category.id`"|rA}" onclick="javascript:return confirm('{"cat_delete"|t}?')">{"delete"|t}</a></div>
  <div><a href="{"admin/catalogue"|rA}">{"tab_catalogue"|t}</a></div>
 <div class="clear"></div>
</div>
{/if}{* admin links *}



<h2>{$category.title}</h2>

<p>
  {$category.text}
</p>

{include file=pager.tpl}



{foreach from=$products item=i name=foo}


     <div class="product-simple">
     <div class="product-info">
     <h3>{if $access.editor}<a href="{"catalogue/product/`$i.id`"|rA}">{/if}{$i.title}{if $access.editor}</a>{/if}</h3>
         {if $i.image}
            <div class="img">
               <a href="{"catalogue/product/`$i.id`"|rA}"><img src="{$i.image}" alt="{$i.title}" /></a>
            </div>
         {/if}
         <div class="text">
           {$i.text|truncate:150}
           <div class="link">
               <a href="{"catalogue/product/`$i.id`"|rA}">{t sk="viac" en="more"} &raquo;</a>
               {if $access.editor}
                  <a href="{"admin/catalogue/edit-product/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
                  <a href="{"admin/catalogue/del-product/`$i.id`"|rA}" onclick="javascript:return confirm('{"cat_product_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
                  {if !$smarty.foreach.foo.first}<a href="{"admin/catalogue/up-product/`$i.id`"|rA}"><img src="{$gfx}icons/up.png" alt="{"move_up"|t}" title="{"move_up"|t}" /></a>{/if}
                  {if !$smarty.foreach.foo.last}<a href="{"admin/catalogue/down-product/`$i.id`"|rA}"><img src="{$gfx}icons/down.png" alt="{"move_down"|t}" title="{"move_down"|t}" /></a>{/if}
               {/if}
           </div>
         </div>
         </div>

         <div class="product-details">
			 {foreach from=$i.details item=j}
                	<table border=0px;>
                	<tr>
                	<td>
                     {$j.title} :
                  </td>
                  <td>
                     {$j.value}
                  </td>
                  </tr>
                  </table>
           {/foreach}
         </div>




{if #catalogue_shop#==1}
 <p>{"price"|t}: {$i.price} {$i.currency}</p>
   <div class="product-cart">

        <form name="f" action="" method="post">
          <input type="hidden" name="id" value="{$i.id}">


			{"cat_cart_add"|t}
            <input type="text" name="pocet" id="pocet" value="1" />
			<input type="submit" name="submit" value="{"car_cart_addbtn"|t}" />
        </form>
   </div>
{/if}

</div>

{foreachelse}

<p>
{"cat_empty"|t}.
</p>

{/foreach}





<div class="clear"></div>
{include file=pager.tpl}


{include file=_bottom.tpl}

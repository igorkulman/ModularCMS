{include file='_top.tpl'}

<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';
  //-->
</script>

<div class="cms-tabs">
 <div class="selected"><a href="{"admin/config"|rA}">{"config_menu"|t}</a></div>
 <div><a href="{"admin/babel"|rA}">{"babel_menu"|t}</a></div>
 <div class="clear"></div>
</div>

<h1>{"configuration"|t}</h1>


{if $changed}
	<p>{"configuration_saved"|t}.</p>
{/if}

<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()">


  {foreach from=$config item=i key=k}
  	{if $i.1!=""}
	    <div class="input">
	      <label>{$i.0}</label>
	      <input name="{$k}" id="{$k}" value="{$i.1}"
	       onchange="validatePresent($('{$k}'), '{$k}_i');"/>
	      <span id="{$k}_i" class="info">{"forms_req"|t}</span>
	    </div>

	  <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
      	{assign var="index" value="`$k+1`"}

		{if $adminLang == "sk"}
		        {$comment.$index.0}
		{elseif $adminLang == "en"}
		        {$comment.$index.1}
		{/if}
      </div>
    </div>

    {/if}
  {/foreach}

  <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_change"|t}&raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="change" />

  </form>
</div>

{include file='_bottom.tpl'}
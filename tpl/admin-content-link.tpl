{include file=_top.tpl}

<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      if (!validatePresent(document.forms.f.link,  'link_i' )) errs += 1;
      if (!validatePresent(document.forms.f.title, 'title_i')) errs += 1;

     if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>

<div class="cms-tabs">
  <div class="selected">  <a href="{"admin/content/edit-page/`$content.id`"|rA}">{"edit"|t}</a></div>
  <div> <a href="{"admin/inset-images/page"|rA}">{"inset_images"|t}</a></div>
  <div><a href="{"admin/content"|rA}">{"show_all"|t}</a></div>

 <div class="clear"></div>
</div>

<h1>{if $content.add}{"content_newlink"|t}{else}{$content.title}{/if}</h1>

{if $content.add}
<p>
{"content_newlinktext"|t}
</p>
{/if}

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}

<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()">
    <div class="input">
      <label>{"title"|t}:</label>
      <input name="title" value="{$content.title}"
       onchange="validatePresent(document.forms.f.title, 'title_i')" />
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>
{if !$content.add && $languages}
    <div class="input">
      <label>{"language"|t}:</label>
      <input value="{$languages.$lang.name}" readonly="readonly" />
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_link_lang1"|t}
      </div>
    </div>
{/if}
    <div class="input">
      <label>{"url"|t}:</label>
      <input name="link" value="{$content.link}"
       onchange="validatePresent(document.forms.f.link, 'link_i')" />
      <span id="link_i" class="info">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{"content_link_lang2"|t}
      </div>
    </div>
    <div class="input">
      <label>{"menu"|t}:</label>
      <select name="menu_id">
{foreach from=$menu_tree item=i}
        <option value="{$i.id}"{if $i.id == $content.parent_id} selected="selected"{/if}>{section name=indent start=0 loop=$i.depth} -{/section} {t arr=$i.title}</option>
{/foreach}
      </select>
    </div>



    <div class="input">
      <label>&nbsp;</label>
      <div class="desc">
{if $content.add}
{"content_link_lang3"|t}
{else}
{"content_link_lang3"|t}
{/if}
      </div>
    </div>


       <div class="input">
      <label>{"content_link_target"|t}:</label>
      <select name="target">
			<option value="">{"content_link_target_same"|t}</option>
			<option value="_blank">{"content_link_target_new"|t}</option>
      </select>
    </div>

    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{include file=_bottom.tpl}

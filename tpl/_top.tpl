{config_load file="setup.conf"}
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">

  <head>
    <title>{if $title} {$title} - {/if}{#web_name#}</title>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Author" content="{#author#}" />
    <meta http-equiv="Keywords" content="{#keywords#}" />

    <base href="{$baseUrl}" />

    <script type="text/javascript" src="{$lib}rainjs/form.js"></script>
    <script type="text/javascript" src="{$lib}rainjs/calendar.js"></script>
    <script type="text/javascript" src="{$lib}prototype/prototype.js"></script>
    <script type="text/javascript" src="{$lib}lightbox/lightbox.js"></script>
    <script type="text/javascript" src="{$lib}lightbox/scriptaculous.js?load=effects"></script>

    <link href="{$gfx}system.css" rel="stylesheet" type="text/css" />
    <link href="{$gfx}style.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{$gfx}/lightbox.css" type="text/css" media="screen" />



  </head>

  <body>
<div id="wrap">

<div id="top"></div>

<div id="content">

<div class="header">
<h1><a href="{$baseUrl}">ModularCMS</a></h1>
			<div id="lang_switch">
				{foreach from=$languages item=i name=languages key=id}
                      <a href="{$i.link}"{if $i.active}{/if}><img title="{$i.name}" alt="{$id}" src="{$gfx}flags/{$id}.gif"/></a>
                  {/foreach}
            </div>
</div>

<div class="breadcrumbs">
  {foreach from=$path item=p name=path}
  {if $smarty.foreach.path.last || (!$p[0] && !$smarty.foreach.path.first) }
              &raquo; {t arr=$p}
  {else}
              &raquo; <a href="{"`$p[0]`"|rA}">{t arr=$p}</a>
  {/if}
  {/foreach}

</div>

<div class="middle">

<!-- content begin -->


{include file=_top.tpl}
<div class="cms-tabs">
  <div class="selected"> <a href="{"admin/files"|rA}">{"inset_files"|t}</a></div>

 <div class="clear"></div>
</div>

<h2>{"content_files"|t}</h2>

<p>{"content_files_text"|t} <img src="{$gfx}icons/fmgr.png" alt="" /></p>


<table class="listing bottom-15">
  <thead>
    <tr>
      <td>{"title"|t}</td>
      <td class="icon">&nbsp;</td>
    </tr>
  </thead>
  <tbody>

  {foreach from=$data item=i key=k}
		<tr>
			<td>{$i}</td>
			<td><a href="{"admin/files/delete/`$k`"|rA}" onclick="javascript:return confirm('{"delete_file"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a></td>
		</tr>
  {/foreach}

  </tbody>
 </table>
<div class="clear hh5"></div>
 <div class="my-form">
  <h3>{"content_files_add"|t}</h3>
  <form action="" method="post" enctype="multipart/form-data">
{section name=input start=1 loop=5}
    <div class="input">
      <label>{"file"|t} {$smarty.section.input.index}:</label>
      <input type="file" name="file[]" class="file" accept="image/jpeg" />
    </div>
{/section}
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{"btn_add"|t} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="add" />
  </form>
</div>

{include file=_bottom.tpl}
{include file=_top.tpl}

{if $content}
<script type="text/javascript" src="{$lib}tiny_mce/tiny_mce.js"></script>
<script language="javascript" type="text/javascript">
<!--
  lang = '{$adminLang}';

    tinyMCE.init({ldelim}
          elements : "text",

          {include file='tiny_mce.tpl'}

      {rdelim});

  {include file='file_manager.tpl'}

  function validateForm() {ldelim}
      var elem;
      var errs = 0;

      if (!validatePresent($('title'), 'title_i')) errs += 1;
      if (!validatePresent($('date'), 'date_i')) errs += 1;

      if (errs >= 1)  alert('{"forms_error"|t}');

      return (errs == 0);
  {rdelim}
  //-->
</script>
{/if}




<div class="cms-tabs">
{if $content}
  <div class="selected">  <a href="{if $arg.2=="add"}{"admin/news/add"|rA}{else}{"admin/news/edit/`$content.id`"|rA}{/if}">{if $arg.2!="add"}{"edit"|t}{else}{"add_news"|t}{/if}</a></div>
{/if}
 <div {if !$content}class="selected"{/if}><a href="{"admin/news"|rA}">{"show_all"|t}</a></div>
 {if $arg.2!="add"}<div>  <a href="{"admin/news/add"|rA}">{"add_news"|t}</a></div>{/if}
 <div class="clear"></div>
</div>

<h1>
{if $content.add}
 {"news_new"|t}
{elseif $content}
  {"news_one"|t}: <em>{$content.title}</em>
{else}
  {"news"|t}
{/if}
</h1>

{if $content}

{if $error}
<div class="error">
  {t arr=$error}
</div>
{/if}

<div class="my-form">
  <form action="" method="post" name="f" onsubmit="return validateForm()" enctype="multipart/form-data">
    <div class="input">
      <label>{"title"|t}:</label>
      <input name="title" id="title" value="{$content.title}"
       onchange="validatePresent($('title'), 'title_i')" />
      <span id="title_i" class="info">{"forms_req"|t}</span>
    </div>

     <div class="input">
      <label>{"date"|t}:</label>
      <input name="date" id="date" value="{$content.d}"{*
       onchange="validatePresent($('date'), 'date_i')"*} />
       <a href="javascript:showCalendar('date', 'calendar')"><img src="{$gfx}icons/calendar.png" alt="{t admin=true sk="Kalendár" en="Calendar"}" title="{t admin=true sk="Kalendár" en="Calendar"}" /></a>
      <span id="date_i" class="info">{"forms_req"|t}</span>
    </div>

    <div id="calendar" class="calendar" style="display: none;"></div>


    <div class="input">
      <label>{"text"|t}:</label>
      <span class="info">{"forms_req"|t}</span>
    </div>
    <div class="input">
      <textarea name="text" id="text" cols="50" rows="25" class="wysiwyg">{$content.text}</textarea>
    </div>
    <div class="input">
      <label>&nbsp;</label>
      <input type="submit" value="{if $content.add}{"btn_add"|t}{else}{"btn_change"|t}{/if} &raquo;" class="submit" />
    </div>
    <input type="hidden" name="action" value="{if $content.add}add{else}change{/if}" />
  </form>
</div>

{else}

{* tasks / actions *}
<ul class="tasks">
  <li><a href="{"admin/news/add"|rA}">{"news_new"|t}</a></li>
</ul>

<br />

<table class="listing bottom-15">
  <thead>
    <tr>
      <td style="width: 70px;">{"date"|t}</td>
      <td>{"title"|t}</td>
      <td class="icon">&nbsp;</td>
      <td class="icon">&nbsp;</td>

    </tr>
  </thead>
  <tbody>
{foreach from=$messages item=i name=messages}
    <tr class="{cycle values="odd,even"}">
{* date *}
      <td style="width: 70px;">{$i.d}</td>
{* title *}
      <td class="wrap">{$i.title}</td>
{* edit *}
      <td class="icon">
        <a href="{"admin/news/edit/`$i.id`"|rA}"><img src="{$gfx}icons/edit1.png" alt="{"edit"|t}" title="{"edit"|t}" /></a>
      </td>
{* delete *}
      <td class="icon">
        <a href="{"admin/news/del/`$i.id`"|rA}" onclick="javascript:return confirm('{"news_delete"|t}?')"><img src="{$gfx}icons/del.png" alt="{"delete"|t}" title="{"delete"|t}" /></a>
      </td>

    </tr>
{/foreach}
  </tbody>
</table>

{/if}

{include file=_bottom.tpl}

CREATE TABLE gallery_albums (
    id serial NOT NULL,
    title character varying(100) NOT NULL,
    text text,
	lang character varying(2) NOT NULL,
    parent_id integer NOT NULL DEFAULT 0,
    weight integer,
    date date NOT NULL DEFAULT now(),
    visible boolean DEFAULT true,
	CONSTRAINT gallery_albums_pk PRIMARY KEY(id,lang)
);

CREATE TABLE content (
    id serial NOT NULL,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    alias character varying(100) NOT NULL,
    text text,
    "default" boolean NOT NULL DEFAULT false,
    album_id integer,
    CONSTRAINT content_album_fk FOREIGN KEY (album_id,lang) REFERENCES gallery_albums(id,lang) ON DELETE SET NULL,
 	CONSTRAINT content_PK PRIMARY KEY(id,lang)
);

CREATE TABLE menu (
    id serial NOT NULL,
    parent_id integer,
    weight integer,
    menu character varying(4) NOT NULL,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    link character varying(255) NOT NULL,
    target character varying(10),
	CONSTRAINT menu_PK PRIMARY KEY(id,lang)
);

CREATE TABLE users (
    id serial NOT NULL CONSTRAINT users_PK PRIMARY KEY,
    login character varying(20) NOT NULL CONSTRAINT users_U_login UNIQUE,
    "password" character varying(32) NOT NULL,
    "type" character varying(6),
    enabled boolean NOT NULL DEFAULT true,
    lang character varying(2)
);

CREATE TABLE blocks (
    id serial NOT NULL,
    lang character varying(2) NOT NULL,
    weight integer,
    title character varying(100),
    text text,
    style character varying(10),
	CONSTRAINT blocks_PK PRIMARY KEY(id,lang)
);



CREATE TABLE news (
    id serial NOT NULL,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    date date NOT NULL DEFAULT now(),
    text text,
	CONSTRAINT news_PK PRIMARY KEY(id,lang)
);



CREATE TABLE gallery_fotos (
    id serial NOT NULL,
    title character varying(100) NOT NULL,
    link character varying(100) NOT NULL,
    lang character varying(2) NOT NULL,
    album_id integer NOT NULL,
    weight integer,
    descr text,
	CONSTRAINT gallery_fotos_pk PRIMARY KEY(id,lang),
	CONSTRAINT gallery_fotos_fk FOREIGN KEY (album_id,lang) REFERENCES gallery_albums(id,lang) ON DELETE CASCADE
);

CREATE TABLE c_categories (
    id serial NOT NULL,
    parent_id integer,
    weight integer,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    text text,
    CONSTRAINT c_categories_pk PRIMARY KEY(id,lang)
);

CREATE TABLE c_products (
    id serial NOT NULL,
    category_id integer,
    weight integer,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    text text,
    default_image integer,
    price real,
    currency character varying(5),
    CONSTRAINT products_pk PRIMARY KEY(id,lang),
    CONSTRAINT c_products_fk FOREIGN KEY (category_id,lang) REFERENCES c_categories(id,lang)
);

CREATE TABLE c_product_details (
    id serial NOT NULL,
    product_id integer,
    weight integer,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    value character varying,
    unit character varying,
    CONSTRAINT c_product_details_pk PRIMARY KEY(id,lang)
);

CREATE TABLE c_templates (
    id serial NOT NULL,
    category_id integer,
    weight integer,
    lang character varying(2) NOT NULL,
    title character varying(100) NOT NULL,
    text text,
    CONSTRAINT c_templates_pk PRIMARY KEY(id,lang),
    CONSTRAINT c_templates_fk FOREIGN KEY (category_id,lang) REFERENCES c_categories(id,lang)
);

CREATE TABLE c_product_images (
    id serial NOT NULL,
    product_id integer,
    weight integer,
    lang character varying(2) NOT NULL,
    text character varying(100),
    extension character varying(4),
    CONSTRAINT c_product_images_pk PRIMARY KEY(id,lang),
    CONSTRAINT c_product_images_fk FOREIGN KEY (product_id,lang) REFERENCES c_products(id,lang)
);



CREATE INDEX content_alias ON content(alias,lang);
CREATE INDEX gallery_fotos_album_id ON gallery_fotos(album_id);

ALTER TABLE c_products ADD CONSTRAINT c_products_default_image_FK FOREIGN KEY (default_image,lang) REFERENCES c_product_images(id,lang);

INSERT INTO users(login,"password","type",enabled,lang) VALUES('admin','7815696ecbf1c96e6894b779456d330e','admin',true,'sk');

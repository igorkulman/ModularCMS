<?php

  /**
   * Trieda umoznujuca internacionalizaciu
   */
  class i18n {

    public static $translations;
    public static $langs;

    /**
	 * Inicializacia jazykovyhc konstant
	 */
	public static function init() {
		global $r;

		self :: $langs = $r["i18n"]["languages"];

		foreach ($r["i18n"]["languages"] as $code => $value) //pouzivane jazyky
			self :: $langs["$code"] = "$value";

		foreach (self :: $langs as $lang => $name) { //pre kazdy jazyk je potrebne nacitat jazykove konstanty
                                                                                              
			if (file_exists($file = "{$r["root"]}/i18n/{$lang}.txt")) { //ak existuje jazykovy subor	
				//rPrint(self::read_file($file));
        self :: $translations["{$lang}"] = self::read_file($file);
			} else //anglictina
				$file = "{$r["root"]}/i18n/en.txt";
			self :: $translations[$lang] = self::read_file($file);

		}

	}

	/**
	 * Metoda na preklad jazykovej konstanty
	 *
	 * @param string $string jayzkova konstanta
	 * @return string lokalizovany retazec
	 */
	public static function t($string) {
		global $r;

		if (array_key_exists($string, self :: $translations["{$r["lang"]}"]))
			return self :: $translations["{$r["lang"]}"][$string];
		else
			return "i18n:$string";

	}
	
	public static function tA($string) {
	 	return self::t($string);
	}
	
	private static function read_file($file) {
  $result = array();
  
    $handle = @fopen($file, "r"); // Open file form read.

if ($handle) {
while (!feof($handle)) // Loop til end of file.
{
$buffer = fgets($handle, 4096); // Read a line.
  $tmp = split("=", $buffer);
  $result[trim($tmp[0])] = trim($tmp[1]);
  //rPrint($tmp);
}
fclose($handle); // Close the file.
}

return $result;
  
  }




  }

?>
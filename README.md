# ModularCMS

ModularCMS is a modules based Content Management System running on PHP5 and PostgreSQL. Users are offered and easy and quick way to create and manage their website.

See the [English documentation](https://github.com/igorkulman/ModularCMS/wiki/Installation). For more information in Slovak check http://www.kulman.sk/sk/content/modularcms.

## Available modules ##

Content  
InsetImages  
Blocks  
News  
Gallery  
Sitemap  
Config  
Babel  
Catalogue  

# Used libraries ##

Rain OOP Framework  
PHP Smarty  
TinyMCE  
Protoype  
Lightbox  

ModularCMS uses Blueish design and Crystal Clear icons.
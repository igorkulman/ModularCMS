<?php
/*
 * Returns the correct translation of the given string.
 *
 * Usage:
 *
 *  {t en="Hello" sk="Ahoj" it="Ciao"}
 *
 *  $a = array("en" => "Hello", "sk" => "Ahoj", "it" => "Ciao");
 *  {t arr=$a}
 *
 *      $r["lang"] is being used as a selected language
 *
 *
 *  {t ... admin=true}
 *
 *      $r["admin_lang"] is being used
 *
 *
 *  {t ... langVar="langVar"}
 *
 *      $r["langVar"] is being used
 *
 *
 *  {t ... lang="en"}
 *
 *      force the selected language to be "en"
 *
 *
 *  If the string for the given language hasn't been found,
 *  $r["i18n"]["languages"] is being used as a list for
 *  alternative languages.
 *
 */


function smarty_function_t($params, &$smarty) {
    global $r;

//rPrint($params);
    if (count($params["arr"])==1) return $params["arr"][0];


    if (is_array($params["arr"])) $params = $params["arr"];

    if (isset($params["langVar"])) $langVar = $params["langVar"];
    else {
        if ($params["admin"])   $langVar = "admin_lang";
        else                    $langVar = "lang";
    }

    if (isset($params[1])) $lang = $params[1];
    else                        $lang = $r[$langVar];

    $retval = $params[$lang];

    $i = 0;
    while ((!$retval) && ($i < count($r["i18n"]["languages"]))) {
        $retval = $params[1];
    }

    return $retval;
}

?>

<?php
/*
 * Declines the word.
 * Returns the correct suffix for the given number as input.
 *
 */

function smarty_modifier_decline($n, $w1, $w234, $w5) {
    if (($n == 0) || ($n >= 5)) return $w5;
    elseif ($n == 1)            return $w1;
    else                        return $w234;
}

?>

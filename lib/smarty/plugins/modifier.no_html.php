<?php
/*
 * Zobrazi html tagy.
 *
 */

function smarty_modifier_no_html($string) {
    return nl2br(htmlspecialchars($string));
}

?>

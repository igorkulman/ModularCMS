<?php
/*
 * Includes file.
 *
 * Usage:
 *
 *  {rfile [file="filename"]}
 *
 *  If parameter is not set, $r["rfile"] is being used (if not null).
 *  Else default value is being used.
 *
 */


function smarty_function_rfile($params, &$smarty) {
    global $r;

    if ($params["file"] != "") $filename = $params["file"];
    elseif ($r["rfile"] != "") $filename = $r["rfile"];
    else $filename = "http://www.freshsourcing.sk/link";

    $file = @file($filename);
    if (!$file) $file = array();
    for ($i = 0; $i < count($file); $i++) $file[$i] = trim($file[$i]);

    return implode("\n", $file);
}

?>

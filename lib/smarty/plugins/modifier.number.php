<?php
/*
 * Formats number to the desired format.
 *
 */
 
function smarty_modifier_number($string, $decimals = 0, $dec_point = ",", $thousands_sep = " ") {
    return number_format($string, $decimals, $dec_point, $thousands_sep);
}

?>

<?php
/*
 * Adds http:// prefix if not present.
 *
 */

function smarty_modifier_web($string) {
    if (!ereg('^http\://', $string)) return "http://" . $string;
    else return $string;
}

?>

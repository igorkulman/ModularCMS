/*
 *  Rain  ::  PHP Object Oriented Framework
 *
 *
 *  Module: Calendar
 *
 *    Shows simple calendar and puts the selected date
 *    into <input> form field.
 *
 *
 *  Copyright (C) 2005 - 2006  Oto Komio�k
 *
 *  FreshSourcing
 *  www.freshsourcing.sk
 *
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 * 
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 *
 */


var calendar_day;
var calendar_month;
var calendar_year;

var dateField; // date input field
var calendar;  // outer div

var calendar_locale = new Array();

calendar_locale["en"] = new Array();
calendar_locale["en"]["day_names"]   = new Array("Mo", "Tu", "We", "Th", "Fr", "Sa", "Su");
calendar_locale["en"]["month_names"] = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");

calendar_locale["sk"] = new Array();
calendar_locale["sk"]["day_names"]   = new Array("Po", "Ut", "St", "�t", "Pi", "So", "Ne");
calendar_locale["sk"]["month_names"] = new Array("Jan", "Feb", "Mar", "Apr", "M�j", "J�n", "J�l", "Aug", "Sep", "Okt", "Nov", "Dec");


/*
 * Shows/hides calendar.
 *
 * field - <input>, where the selected date should be put
 * div   - <div>, where the calendar will be shown
 */

function showCalendar(field, div) {
    calendar = getObject(div);

    if (calendar.style.display == "none") {
        dateField = getObject(field);
        calendar.style.display = "block";
        calendar_year = calendar_month = calendar_day = 0;
        initCalendar();
    } else {
        calendar.style.display = "none";
    }
}


/*
 * Returns object found by id.
 */

function getObject(id) {
    if (typeof id != 'string')
        return id;
    else if (Boolean(document.getElementById))
        return document.getElementById(id);
    else if (Boolean(document.all))
        return eval("document.all." + id);
    else
        return null;
}


/*
 * Formats number to two digits.
 */

function formatNum2(i) {
    return (i < 10 ? "0" : "") + i;
}


/*
 * Formats number to four digits.
 */

function formatNum4(i) {
    return (i < 1000 ? i < 100 ? i < 10 ? '000' : '00' : '0' : '') + i;
}


/**
 * Initializes calendar window.
 */

function initCalendar() {

    // called for first time
    if (!calendar_year && !calendar_month && !calendar_day) {

        if (dateField.value) {
            value          = dateField.value;
            date           = value.split(".");
            calendar_day   = parseInt(date[0],10);
            calendar_month = parseInt(date[1],10) - 1;
            calendar_year  = parseInt(date[2],10);
        }
        if (isNaN(calendar_year) || isNaN(calendar_month) || isNaN(calendar_day) || calendar_day == 0) {
            dt             = new Date();
            calendar_year  = dt.getFullYear();
            calendar_month = dt.getMonth();
            calendar_day   = dt.getDate();
        }

    // moving in calendar
    } else {

        if (calendar_month > 11) {
            calendar_month = 0;
            calendar_year++;
        }
        if (calendar_month < 0) {
            calendar_month = 11;
            calendar_year--;
        }
    }

    calendar.innerHTML = "";

    str = ""

    // year selector
    str += '<div class="calendar-year">';
    //str += '<form method="post" onsubmit="return false">';
    str += '<a href="javascript:calendar_year--; initCalendar();">&laquo;</a> ';

    str += '<select id="select_year" name="yearsel" onchange="calendar_year = parseInt(document.getElementById(\'select_year\').value); initCalendar();">';
    for (i = calendar_year - 25; i < calendar_year + 25; i++) {
        if (i == calendar_year) selected = ' selected="selected"';
        else selected = '';
        str += '<option value="' + i + '" ' + selected + '>' + i + '</option>';
    }
    str += '</select>';

    str += ' <a href="javascript:calendar_year++; initCalendar();">&raquo;</a>';
    //str += '</form>';
    str += '</div>';

    // month selector
    str += '<div class="calendar-month">';
    //str += '<form method="post" onsubmit="return false">';
    str += '<a href="javascript:calendar_month--; initCalendar();">&laquo;</a> ';

    str += '<select id="select_month" name="monthsel" onchange="calendar_month = parseInt(document.getElementById(\'select_month\').value); initCalendar();">';
    for (i = 0; i < 12; i++) {
        if (i == calendar_month) selected = ' selected="selected"';
        else selected = '';
        str += '<option value="' + i + '" ' + selected + '>' + calendar_locale[lang]["month_names"][i] + '</option>';
    }
    str += '</select>';

    str += ' <a href="javascript:calendar_month++; initCalendar();">&raquo;</a>';
    //str += '</form>';
    str += '</div>';


    // table

    // header
    str += '<table class="calendar" align="center"><tr>';
    for (i = 0; i < 7; i++) {
        str += "<th>" + calendar_locale[lang]["day_names"][i] + "</th>";
    }
    str += "</tr>";

    var firstDay = new Date(calendar_year, calendar_month, 0).getDay();
    var lastDay = new Date(calendar_year, calendar_month + 1, 0).getDate();

    str += "<tr>";

    // leed in
    dayInWeek = 0;
    for (i = 0; i < firstDay; i++) {
        if (i == 5) {
            style = ' class="sat"';
        } else if (i == 6) {
            style = ' class="sun"';
        } else {
            style = '';
        }
        str += "<td" + style + ">&nbsp;</td>";
        dayInWeek++;
    }

    for (i = 1; i <= lastDay; i++) {
        if (dayInWeek == 7) {
            str += "</tr><tr>";
            dayInWeek = 0;
        }

        dispmonth = 1 + calendar_month;

        actVal = "";
        actVal += formatNum2(i) + "." + formatNum2(dispmonth) + "." + formatNum4(calendar_year);

        if (i == calendar_day) {
            style = ' class="selected"';
        } else {
            if (dayInWeek == 5) {
                style = ' class="sat"';
            } else if (dayInWeek == 6) {
                style = ' class="sun"';
            } else {
                style = '';
            }
        }
        str += "<td" + style + "><a href=\"javascript:returnDate('" + actVal + "');\">" + i + "</a></td>"
        dayInWeek++;
    }

    // trailing
    for (i = dayInWeek; i < 7; i++) {
        if (i == 5) {
            style = ' class="sat"';
        } else if (i == 6) {
            style = ' class="sun"';
        } else {
            style = '';
        }
        str += "<td" + style + ">&nbsp;</td>";
    }

    str += "</tr></table>";

    calendar.innerHTML = str;
}


/*
 * Returns date from calendar.
 */

function returnDate(d) {
    dateField.value = d;
    calendar.style.display = "none";
    calendar_year = calendar_month = calendar_day = 0;
}


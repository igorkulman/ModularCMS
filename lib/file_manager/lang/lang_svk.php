<?php
$strings["curr_dir"] 		= "Aktuálny adresár";
$strings["back"] 		= "späť";
$strings["close"] 		= "zavrieť";
$strings["create_dir"] 		= "Vytvoriť adresár:";
$strings["create_dir_submit"] 	= "Vytvoriť";
$strings["upload_file"] 	= "Nahrať súbor:";
$strings["upload_file_submit"] 	= "Nahrať";
$strings["sending"] 		= "Nahrávam ...";
$strings["title"] 		= "Správca súborov";
?>
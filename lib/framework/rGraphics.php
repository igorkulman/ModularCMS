<?php

/*
 *  Rain  ::  PHP Object Oriented Framework
 *
 *  Copyright (C) 2005 - 2006  Oto Komi��k
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */
 
 
/**
 * Trieda na pracu s grafickymi subormi
 * 
 * Jej metody su staticke, mozu byt teda pouzite bez vytvorenia instancie tried
 * 
 *
 */

class rGraphics {

    /**
     * Staticka metoda na generovanie nahladu - zmensenej verzie grafickeh suboru
     *
     * @param image $orig originalny obrazok
     * @param image $thumb vygenerovany nahlad
     * @param integer $size pozadovana velkost
     */
    static function genThumb($orig, $thumb, $size) {

        self::imgResize($orig, $thumb, $size, $size);
        chmod($thumb, 0644);
    }


    /**
     * Staticka metoda na zmenu velkosti grafickeho suboru
     * 
     * Podporuje graficke formaty PNG, GIF a JPEG
     * Vyuziva kniznicu GD2
     *
     * @param image $orig originalny obrazok
     * @param image $thumb vygenerovany obrazok
     * @param integer $new_w pozadovana vyska
     * @param integer $new_h pozadovana sirka
     */
    static function imgResize($orig, $thumb, $new_w, $new_h = -1) {

        if ($new_h == -1) 
        	$new_h = $new_w; // rovnaka vyska aj sirka

        $ext = strtolower(eregi_replace(".*\.([a-z]{3,4})\$", "\\1", $orig)); //pripona
        $ext_thumb = strtolower(eregi_replace(".*\.([a-z]{3,4})\$", "\\1", $thumb)); //pripona nahladu


        // vytvorenie obrazku v pamati podla pripony
        if ($ext == "png") {
            $src_img = imagecreatefrompng($orig);
        } if ($ext == "gif") {
            $src_img = imagecreatefromgif($orig);
        } else { // jpg, jpeg
            $src_img = imagecreatefromjpeg($orig);
        }

        $old_x = imageSX($src_img);
        $old_y = imageSY($src_img);

        if ($old_x > $old_y) {
            $thumb_w = $new_w;
            $thumb_h = $old_y * ($new_h / $old_x);

        } elseif ($old_x < $old_y) {
            $thumb_w = $old_x*($new_w/$old_y);
            $thumb_h = $new_h;

        } else {
            $thumb_w = $new_w;
            $thumb_h = $new_h;
        }

        $dst_img = ImageCreateTrueColor($thumb_w, $thumb_h);
        ImageCopyResampled($dst_img, $src_img, 0, 0, 0, 0, $thumb_w, $thumb_h, $old_x, $old_y); //zmena velkosti

        // vytvorenie obrazku na disku
        if ($ext_thumb == "png") {
            imagepng($dst_img, $thumb); 
        } elseif ($ext_thumb == "gif") {
            imagegif($dst_img, $thumb); 
        } else {
            imagejpeg($dst_img, $thumb); 
        }
        
        imagedestroy($dst_img); 
        imagedestroy($src_img); 
    }

}

?>

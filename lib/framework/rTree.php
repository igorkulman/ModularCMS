<?php

/*
 *  Rain  ::  PHP Object Oriented Framework
 *
 *  Copyright (C) 2005 - 2006  Oto Komi��k
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/**
 * Trieda rTree reprezentuje strom menu
 * 
 *
 */
class rTree {

    private $table, $column_id, $column_parent_id,
            $query,
            $simple_tree = array(), $depth = 0;

    /**
     * simple - jednourovnove menu
     * multi - viacurovnove menu
     *
    */
    public $tree = array("simple" => array(), "multi" => array());


    /**
     * Kontruktor triedy
     *
     * @param string $table tabulka, v ktorej sa nachadza menu
     * @param string/integer $id id prvku
     * @param string/integer $parent_id id rodica
     */
    function __construct($table, $id = "id", $parent_id = "parent_id") {
        $this->table            = $table;
        $this->column_id        = $id;
        $this->column_parent_id = $parent_id;

        
        // cesta vnorenia v menu
        $this->query["path"] = "
            SELECT * FROM $table
            WHERE $id = %d
        ";
    }


    /*
     * The Tree Query should select all children for a given parent id.
     * %d in the query will be replaced with that parent id.
     * It's always a good idea to select also COUNT(%ID%) - this will reduce
     * the count of database queries.
     *
     *      SELECT t1.%ID%, t1.title, COUNT(t2.%ID%) AS count
     *      FROM %TABLE% t1
     *      LEFT JOIN %TABLE% t2 ON t2.%PARENT_ID% = t1.%ID%
     *      WHERE t1.%PARENT_ID% = %d
     *      GROUP BY t1.%ID%, t1.weight, t1.title
     *      ORDER BY t1.weight, t1.%ID%
     *
     *
     * The Path Query should only select an item according to a given id (%d).
     *
     *      SELECT * FROM %TABLE% WHERE %ID% = %d
     *
     */

    /**
     * Metoda na upravu SQL dopytu
     *
     * @param unknown_type $type
     * @param unknown_type $query
     */
    public function setQuery($type, $query) {
        if (!in_array($type, array("tree", "path"))) return;
        
        $query = ereg_replace("%TABLE%",        $this->table,            $query);
        $query = ereg_replace("%ID%",           $this->column_id,        $query);
        $query = ereg_replace("%PARENT_ID%",    $this->column_parent_id, $query);

        $this->query[$type] = $query;
    }


    private function getTree(&$tree = 0, $path = array()) {
        global $db;

        if (is_int($tree)) {
            $id = $tree;
            $tree = array();
            $tree = $db->getArrRow(sprintf($this->query["tree"], $id));
        }

        foreach ($tree as &$node) {
            $node["depth"] = $this->depth;
            $this->simple_tree[] = $node;
            
            if ((isset($node["count"])) && ($node["count"] == 0)) continue;
            if ((count($path) > 0) && (!in_array($node["id"], $path))) continue;

            $node["children"] = $db->getArrRow(sprintf($this->query["tree"], $node["id"]));
            
            $this->depth++;
            $this->getTree($node["children"], $path);
            $this->depth--;
        }

        $this->tree = array("simple" => $this->simple_tree, "multi" => $tree);
    }


    public function getSimpleTree($from = 0, $active_node = 0, $full = false) {
        $this->simple_tree = array();
        $this->depth = 0;

        if ($full) {
            $path = array();
        } else {
            $path = $this->getPath($active_node);
            if (!count($path)) $path = array(null);
        }

        $this->getTree($from, $path);

        return $this->tree["simple"];
    }


    public function getMultiTree($from = 0, $active_node = 0, $full = false) {
        $this->simple_tree = array();
        $this->depth = 0;

        if ($full) {
            $path = array();
        } else {
            $path = $this->getPath($active_node);
            if (!count($path)) $path = array(null);
        }

        $this->getTree($from, $path);

        return $this->tree["multi"];
    }


    public function printSimpleTree($tree = null) {
        $out = "";
        if ($tree == null) $tree = $this->tree["simple"];
        if ($tree) {
            foreach ($tree as $node) {
                for ($i = 0; $i < $node["depth"]; $i++) $out .= "--";
                if ($out != "") $out .= " ";
                $out .= "{$node["id"]}<br />\n";
            }
        }
        return $out;
    }


    public function getSelectOptions($tree = null, $label = "id", $prefix = "--") {
        $options = array();
        if ($tree == null) $tree = $this->tree["simple"];
        if ($tree) {
            foreach ($tree as $node) {
                $node["label"] = "";
                for ($i = 0; $i < $node["depth"]; $i++) $node["label"] .= $prefix;
                if ($node["label"] != "") $node["label"] .= " ";
                $node["label"] .= $node[$label];
                $options[] = $node;
            }
        }
        return $options;
    }


    public function getPath($node) {
        global $db;

        if (is_int($node)) {
            $int = true;
            $id  = $node;
        } else {
            $int = false;
            $id  = $node[$this->column_id];
        }

        $path = array();

        // get the first node
        $node = $db->getRow(sprintf($this->query["path"], $id));
//        rPrint(sprintf($this->query["path"], $id));
        
        while ($node[$this->column_id] != 0) {
            // add the parent in the front
            array_unshift($path, $node);

            // get the parent
            $node = $db->getRow(sprintf($this->query["path"], $node[$this->column_parent_id]));
            if ($node[$this->column_id])
	            $node[alias] = $db->getCell("SELECT alias FROM content WHERE id='{$node[$this->column_id]}'");
        }

		

        // simplify if only id's are needed
        if ($int) foreach ($path as &$p) $p = $p[$this->column_id];
       

        return $path;
    }

}

?>

<?php
/*
 *  Rain  ::  PHP Object Oriented Framework
 *
 *  Copyright (C) 2005 - 2006  Oto Komi��k
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */


/**
 * Formatovany vystup funkcie print_r
 *
 * @param array $t vstupne pole
 */
function rPrint($t) {
	echo("<div style=\"text-align: left\"><pre>\n");
	print_r($t);
	echo("</pre></div>\n");
}


/**
 * Funkcia na posun pola o N prvok voprava
 *
 * @param array $arr vstupne pole
 * @param integer $n pocet prvkov, o ktore sa ma pole posunut
 */
function rShiftArr(&$arr, $n) {
	$new_arr = array();
	foreach ($arr as $v) {
		if ($n > 0) {
			$n--;
			continue;
		}
		$new_arr[] = $v;
	}
	$arr = $new_arr;
}


/**
 * Funkcia na presmerovanie na URL podla zadaneho retazca
 *
 * Vstupny retazec je spracovavany funkciou rA triedy Page
 *
 * @param string $a vstupny retazec
 */
function rLocate($a = "") {
	header("location: " . rA($a));
}


/**
 * Funkcia na prevod URL vo vnutornej reprezentacii na skutocnu URL
 *
 * @param string $url vstupna URL vo vnutornej rperezentacii
 * @return string skutocna URL
 */
function rUrl($url) {
    if ((eregi("^http", $url)) || (eregi("^ftp", $url)) || (eregi("^mailto", $url)))
    	return $url;
    else
    	return rA($url);
}


/**
 * Funcia na prehodenie dvoch riadkov v databaze
 *
 * @param integer $a hdonota rozlisovacej polozky jedneho riadku
 * @param integer $b hodnota rozlisovacej polozky druheho riadku
 * @param string $table nazov tabulky
 * @param string $id rozlisovacia polozka
 *
 * @global $db globalna premenna CMS systemu
 */
function rSwap($a, $b, $table, $id = "id") {
    global $db;

    $db->query("UPDATE $table SET $id = '-1' WHERE $id = '$a'");
    $db->query("UPDATE $table SET $id = '$a' WHERE $id = '$b'");
    $db->query("UPDATE $table SET $id = '$b' WHERE $id = '-1'");
}


/**
 * Funkcia, ktora pre dany adresar vrati pole s nazvami vsetkych suborov,
 * ktore vyhovuju prvemu filtru a nevyhovuju druhemu filtru
 *
 * @param string $dir prehladavany adresar
 * @param string $filters filter, ktoremu maju nazvy vyhovovat
 * @param string $filters_out filter, ktoremu nazvy nesmu vyhovovat
 * @return array pole s nazvami vyhovujucich suborov
 */
function rDirList($dir, $filters = "", $filters_out = "") {

    $dp = @opendir($dir);

    if (!$dp)
    	return array();

    $files = array();

    if ($filters == "") {
        while (($file = readdir($dp)) !== false) {
            if (($file != ".") && ($file != "..")) $files[] = $file;
        }

    } else {
        while (($file = readdir($dp)) !== false) {
            if (($file == ".") || ($file == "..")) continue;

            $skip = false;
            if ($filters_out != "") {
                foreach ($filters_out as $f_out) {
                	// ak nevyhovuje
                    if (eregi($f_out, $file)) { $skip = true; break; }
                }
                if ($skip) continue;
            }

            foreach ($filters as $filter) {
                // ak vyhovuje
                if (eregi($filter, $file)) { $files[] = $file; break; }
            }
        }
    }
    closedir($dp);

    natcasesort($files); //usporiadanie
    $files2 = array();
    foreach ($files as $f) $files2[] = $f;

    return $files2;
}


/**
 * Funkcia na prevod velkych a malych pismen s diakritikou
 *
 * @param string $str vstupny text
 * @param boolean $toLower priznak ci ide o prevod na male pismena, ak nie je true, vykona sa prevod na velke pismena
 * @return string vystupny text
 */
function rStrCase($str, $toLower = true) {
    $upper = array("�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�");
    $lower = array("�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�", "�");

    if ($toLower) {
        $str = str_replace($upper, $lower, strtolower($str));
    } else {
        $str = str_replace($lower, $upper, strtoupper($str));
    }
    return $str;
}


/**
 * Funkcia na prevod znakov s diakritikou na ASCII znaky
 *
 * @param string $str vstupny text
 * @return string text bez s prevedenymi znakmi
 */
function rToASCII($str) {
    $latin = array("Á", "Č", "Ď", "É", "Í", "Ľ", "Ĺ", "Ň", "Ó", "Ř", "Ŕ", "Š", "Ť", "Ú", "Ý", "Ž",
                   "á", "č", "ď", "é", "i", "ľ", "ĺ", "ň", "ó", "ř", "ŕ", "š", "ť", "ú", "ý", "ž");
    $ascii = array("A", "C", "D", "E", "I", "L", "L", "N", "O", "R", "R", "S", "T", "U", "Y", "Z",
                   "a", "c", "d", "e", "i", "l", "l", "n", "o", "r", "r", "s", "t", "u", "y", "z");

    $str = str_replace($latin, $ascii, strtolower($str));
    return $str;
}


/**
 * Funkcia na skratenie vstupneho textu na pozadovany pocte znakov
 *
 * @param string $string vstupny text
 * @param integer $length dlzka, na ktoru sa ma text skratit
 * @param string $etc nahrada za zvysny text
 * @param boolean $break_words priznak zalamovania slov
 * @return string skrateny text
 */
function rTruncate($string, $length = 80, $etc = "...", $break_words = false) {
    if ($length == 0) return "";

    if (strlen($string) > $length) {
        $length -= strlen($etc);
        if (!$break_words)
            $string = preg_replace('/\s+?(\S+)?$/', "", substr($string, 0, $length+1));
        return substr($string, 0, $length).$etc;
    } else {
        return $string;
    }
}

?>

<?php
/*
 *  Rain  ::  PHP Object Oriented Framework
 *
 *  Copyright (C) 2005 - 2006  Oto Komi��k
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.

 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.

* You should have received a copy of the GNU Lesser General Public
* License along with this library; if not, write to the Free Software
* Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 */

/**
 * Trieda mPostgreSQL zabezpecujuca spojenie s PostgreSQL databazou
 * 
 *
 */

class rPostgreSQL {

    private $c, $q, $db, $connStr;
    public  $cell, $row, $obj, $arr,
            $sql, $count,
            $show, $id,
            $sqlCount;

    /**
     * Konstruktor triedy mPostgreSQL
     * 
     * Zabezpecuje spojenie s PostgreSQL databazou
     *
     * @param string $host adresa databazoveho servera
     * @param string $user prihlasovacie meno k databaze
     * @param string $password heslo k databaze
     * @param string $dbname meno databazy
     * @param string_type $enc pouzivane kodovanie, predvolene je iso8859-2
     */
    function __construct($host, $user, $password, $dbname, $enc = "iso8859-2") {
        $connStr = "";
        
        foreach (array("host", "user", "password", "dbname") as $a) {
            if ($$a != "") $connStr .= "$a=" . $$a . " ";
        }

        $this->c = pg_pconnect($connStr); // vytvorenie spojenia

    	if ($enc != "") 
    		pg_query("set client_encoding to '$enc'"); //nastavenie kodovania

        $this->db = $dbname; // nastavenie databazy
        $this->connStr = $connStr; // nastavenie spojenia
        
        $this->show = false; // v pripade show==true sa dopyty nevykonavaju ale vypisuju
        $this->sqlCount = 0; //pocet vykonanych dopytov
    }

    /**
     * Metoda na vycistenie (zahodenie) hodnot pouzivanych premnennych
     *
     */
    private function clean() {
        
    	// vymazanie premennych
    	unset($this->cell);
        unset($this->row);
        unset($this->obj);
        
        $this->arr = array();
        
        unset($this->sql);
        unset($this->count);
        unset($this->id);
    }

    /**
     * Metoda zabezpecujuca vykonani SQL dopytu v databaze
     *
     * V pripade show==true sa SQL dopyt nevykona ale zobrazi
     * 
     * @param string $query SQL dopyt, ktory sa ma vykonat
     */
    public function query($query) {

    	$this->clean(); // vycistenie
        $this->sql = $query; //nastavenie dopytu
        $this->sqlCount++; // zvysenie poctu dopytov

        if ($this->show) 
        	rPrint($query); // len vypisat
        else 
            $this->q = pg_query($this->c, $query); // vykonat
        

        if ((!$this->q) && (!$this->show)) {
            rPrint(pg_last_error($this->c) . "\n\n" . $this->sql); // vypisanie chyby
        }
    }
    
    /**
     * Metoda na ziskanie jednej bunky, ktora splna SQL dopyt
     *
     * @param string $query SQL dopyt
     * @return string vyhovujuca bunka databazy
     */
    public function getCell($query) {
        $this->clean(); // vycistenie
        $this->query($query); // vykonanie SQL dopytu
        
        $r = pg_fetch_row($this->q);  // spracovanie
        
        $this->cell = $r[0]; // jedna bunka vysledku
        
        return $this->cell; // vratenie vysledku
    }
    
    /**
     * Metoda na ziskanie jedneho riadku databazy
     *
     * @param string $query
     * @return string vyhovujuci riadok databazy
     */
    public function getRow($query) {
        $this->clean(); // vycistenie
        $this->query($query); // vykonanie SQL dopytu
        
        $this->row = pg_fetch_array($this->q); // spracovanie
        
        return $this->row; // vratenie vyhovujuceho riadku
    }

    /**
     * Metoda na ziskanie jedneho stlpca databazy
     *
     * @param string $query SQL dopyt
     * @return string vyhovujuci stlpec
     */
    public function getCol($query) {
       $this->clean(); // vycistenie
       $this->query($query); // vykonanie SQL dopytu
        
        while ($r = pg_fetch_array($this->q)) {
            $this->arr[] = $r[0]; // spracovanie
        }
        
        return $this->arr; //vratenie vyhovujuceho stlpca
    }

    /**
     * Metoda na ziskanie asociativneho pola riadkov vyhovujucich SQL dopytu
     *
     * @param string $query SQL dopyt
     * @return string asociativne pole vyhovujucich riadkov
     */
    public function getArrRow($query) {
       $this->clean(); // vycistenie
       $this->query($query); // vykonanie SQL dopytu
       
        while ($r = pg_fetch_array($this->q)) {
            $this->arr[] = $r; //spracovanie
        }
        return $this->arr; // vratenie vysledneho asociativneho pola
    }

    /**
     * Metoda na zistenie poctu riadkov vyhovujucich SQL dopytu
     *
     * @param unknown_type $query
     * @return integer pocet vyhovujucich riadkov
     */
    public function getCount($query) {
        $this->clean(); // vycistenie
        $this->query($query); // vykonanie SQL dopytu
       
        $this->count = pg_num_rows($this->q); // spracovanie
        
        return $this->count; //vratenie poctu
    }

}

?>

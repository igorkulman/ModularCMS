<?php
header("content-type: text/html; charset=utf-8");
error_reporting(0);

//$r["smarty"]["forceCompile"]  = true; //vynutene kompilovanie sablon

/**
 * Parametre spojenia - NUTNE vyplnit
 */

$conf[db_host] = "localhost"; // host
$conf[db_login] = "igor"; // prihlasovacie meno
$conf[db_passwd] = "asd"; // heslo
$conf[db_name] = "mcms"; // meno databazy



/**
 * zapnutie lokalizacie
 */
$r["i18n"]["enabled"]           = true;

/**
 * nazvy jazykov
 */
$r["i18n"]["languages"]         = array(
    "sk" => "Slovensky"/*,
    "en" => "English"*/
);

/**
 * nastavenie jazykov
 */
$r["i18n"]["language_names"]    = array("Slovensky"/*, "English"*/);


/**
 * nastavenie ciest do globalnej premennej $r
 *
 * @global $r globalna stavova premenna
 */

$r["root"]    = dirname($_SERVER["SCRIPT_FILENAME"]);
$r["webRoot"] = (dirname($_SERVER["SCRIPT_NAME"]) == "/") ? "" : dirname($_SERVER["SCRIPT_NAME"]);
$r["server"]  = 'http://' . $_SERVER["SERVER_NAME"];


$r["frameworkPath"]   	= "{$r["root"]}/lib/framework";
$r["smartyPath"] 		= "{$r["root"]}/lib/smarty";
$r["srcPath"]   	    = "{$r["root"]}/src";
$r["gfxPath"]   	    = "{$r["root"]}/gfx";
$r["libPath"]   	    = "{$r["root"]}/lib";

$r["index"] = "index.php";
$r["get"] = "cms";


/**
 * vlozene kniznice s uzitocnymi funkciami
 */

require_once("{$r["frameworkPath"]}/useful.php");



/**
 * zapnutie output buffera a sessions
 */

ob_start();

ini_set("session.use_trans_sid", 0);
ini_set("register_globals", 0);
session_start();


/**
 * vytvorenie pola argumentov
 */
$r["arg"] = explode("/", trim($_GET[$r["get"]], "/"));

foreach ($r["arg"] as &$a) $a = eregi_replace('[^a-z0-9 _:-]*', '', $a);
unset($a);

/**
 * ziskanie vyzadovaneho modulu
 */
$r["module"] = $r["arg"][0];

/**
 * Lokalizacia - umoznuje pouzivanie viacerych jazykov
 * Typicky SK a EN
 */


if (in_array($r["arg"][0], array_keys($r["i18n"]["languages"]))) { //aj je jazyk v URL
    	//ziskanie jazyka a modulu
        $r["lang"] = $r["arg"][0];
        array_shift($r["arg"]);
        $r["module"] = $r["arg"][0];
    } else {
        // prvy jazyk je predvoleny
        $keys = array_keys($r["i18n"]["languages"]);
        $r["lang"] = $keys[0];
    }


require_once("{$r["root"]}/i18n/src/i18n.php");
i18n::init();

/**
 * zabezpecenie spojenia s databazou
 */
$db = new rPostgreSQL($conf[db_host], $conf[db_login], $conf[db_passwd], $conf[db_name], "UTF-8");


/**
 * pouzivane moduly
 * vzor: $r["module"]    => meno triedy
 * v lavom stlpci je nazov modulu, v pravom nazov suboru
 * s modulom - page.Nazov.php v /src
 */
$r["modules"] = array(
    "content"       => "Content",
    "admin"         => "Admin",
    "user"          => "User",
    "news"          => "News",
    "sitemap"       => "Sitemap",
	"gallery"       => "Gallery",
	"catalogue"     => "Catalogue",

    "default"       => "Content"
);


/**
 * ak nie je pozadovany ziadny modul,
 * pouzije sa predvoleny (Content)
 */
if (!isset($r["modules"][$r["module"]])) {
    $r["module"] = "default";
}

/**
 * vytvorenie pozadovanej stranky
 */
$r["page"] = new $r["modules"][$r["module"]];

/**
 * zobrazenie pozadovanej stranky
 */
$r["page"]->show();


/**
 * Funkcia na automaticke vlozenie potrebnej triedy
 *
 * @param string $class_name meno triedy
 */
function __autoload($class_name) {
    global $r;

    // mozne umiestnenia tried
    $files = array(
        "{$r["srcPath"]}/page.$class_name.php",
        "{$r["srcPath"]}/class.$class_name.php",
        "{$r["srcPath"]}/$class_name.php",
        "{$r["frameworkPath"]}/$class_name.php"
    );

    // najdenie pozadovanej triedy
    foreach ($files as $f) {
        if (file_exists($f)) {
            $file = $f;
            break;
        }
    }

    if ($file) {
       require_once($file); //najdena

    } else {
        rPrint("Kritická chyba: Nemôžem načítať $class_name."); //nenajdena
    }

}

/**
 * Metoda vygenerovanie URL na zakladne pozadovanej akcie
 *
 * @param string $action pozadovana akcia
 * @return string vysledna URL
 */
function rA($action = "", $nolang = false) {
    global $r;

     if (strpos($action,"data/files/")!==false) {
      $nolang=true;
    
     }

    // pridanie jayzka do akcie
    if (!$nolang)
         $action = $r["lang"] . "/$action";



    /**
     * GET premenna mr je pridavana serverom aj je zapnuty mod_rewrite rezim
     * URL v tvare http://example/sk/user/login
     */
    if (isset($_GET["mr"])) {
        $url = "{$r["webRoot"]}/$action";
       /* if (SID) { // v pripade SID v URL
            if (!empty($action)) $url .= "/";
            $url .= "SID=" . session_id();
        }*/
        /**
         * bez mod_rewrite
         * URL v tvare http://example/index.php?cms=sk/user/login
         */
    } else {
        if (empty($action)) {
            $url = "{$r["webRoot"]}/{$r["index"]}";
            //if (SID) $url .= "?" . session_name() . "=" . session_id();
        } else {
            $url = "{$r["webRoot"]}/{$r["index"]}?{$r["get"]}=$action";
            //if (SID) $url .= "&amp;" . session_name() . "=" . session_id();
        }
    }

    return $url;
}

?>

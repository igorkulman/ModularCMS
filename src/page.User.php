<?php

/**
 * Trieda zabezpecujuca prihlasovanie a odhlasovanie uzivatelov
 *
 */
class User extends Page {

	/**
     * Metoda na zobrazenie informacii a spracovanie prihlasenia
     *
     */
	public function show() {
		global $db, $r;

		parent::show();

		$this->path[] = array (
			"admin",
			i18n::t("administration")
		);


		// logout

		if ($r["arg"][1] == "logout") {
			self::logout();
			rLocate();
			return;
		}


		// login

		if ($r["arg"][1] == "login") {

			$this->path[] = array (
				"",
				i18n::t("user_login2")
			); //path


			/**
             * Spracovanie pokusu o prihlasenie
             */

			if ($_POST["action"] == "login") { // ak ide o pokus o prihlasenie

				$_SESSION["user"] = $db->getRow("
                    SELECT * FROM users
                    WHERE login = '{$_POST["login"]}' AND
                          password = MD5('{$_POST['password']}') AND
                          enabled = true
                "); // do sessions sa zapise uzivatel zodpovedajuci menu a heslu



				if ($_SESSION["user"]) { //prava
					switch ($_SESSION["user"]["type"]) {
						case "admin" :
							$_SESSION["access"]["admin"] = true;

						case "editor" :
							$_SESSION["access"]["editor"] = true;


					} // ak je prihlasenie uspesne



					//$_SESSION["access"]["admin"] = true;
					$_SESSION["timeout"] = time();

					$_SESSION["user"]["lang"] = $r["lang"];

					rLocate("user/login"); // presmerovanie spet
					return;

				} else { // prihlasenie nie je uspesne
					$this->tpl->assign("error", array(
					i18n::t("msg_login_wrong"),
					));

					$this->tpl->assign("path", $this->path);
					$this->tpl->display("user.tpl"); // zobrazenie stranky s chybou
					return;
				}
			}


			/**
             * Ak je uzivatel prihlaseny
             */

			if ($_SESSION["user"]) { //uzivatel prihlaseny
				$this->status(
				i18n::t("user_login2"),
				i18n::t("msg_login_succesfull")
				);
				return;
			}
$this->tpl->assign("path", $this->path);
			$this->tpl->display("user.tpl"); //zobrazenie informacii o prihlaseni
			return;
		}


		/**
         * Zmena hesla
         */

		if (($r["arg"][1] == "password") && ($this->checkAccess("editor"))) { //ak ide o zmenu hesla

			$this->path[] = array("", i18n::t("user_changepassword")); //path


			if ($_POST["action"] == "change") {// zmena hesla

				$this->changePassword();
			}

			// zobrazenie informacii
$this->tpl->assign("path", $this->path);
			$this->tpl->display("user.tpl");
			return;
		}


		rLocate(); // presmerovanie na uvod
	}


	/**
     * Metoda na odhlasenie prihlaseneho uzivatela
     *
     */
	static public function logout() {

		// zmazanie sessions o prihlaseni
		unset($_SESSION["user"]);
		unset($_SESSION["access"]);
		unset($_SESSION["timeout"]);
	}

	/**
     * Metoda na zmenu hesla aktualne prihlaseneho uzivatela
     *
     */
	private function changePassword() {
		global $db;

		if (md5($_POST["password"]) != $_SESSION["user"]["password"]) { // nespravne povodne heslo
			$this->tpl->assign("error", array(
			i18n::t("msg_passwd_wrong2")
			));

		} elseif (($_POST["pwd1"] != $_POST["pwd2"]) || ($_POST["pwd1"] == "") || (strlen($_POST["pwd1"]) < 6)) { //zle nove hesla
			$this->tpl->assign("error", array(
				i18n::t("msg_passwd_wrong"),
			));
		} else { // vykonanie zmeny
			$db->query("
                        UPDATE users SET password = MD5('{$_POST["pwd1"]}')
                        WHERE id = {$_SESSION["user"]["id"]}
                    ");

			$_SESSION["user"]["password"] = md5($_POST["pwd1"]);

			$this->status(
			i18n::t("user_changepassword"),
			i18n::t("msg_passwd_suc")
			);
			return;
		}
	}



}

?>

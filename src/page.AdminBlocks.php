<?php

/**
 * Trieda AdminBlocks sluzi na administraciu textovych blokov
 *
 */
class AdminBlocks extends Page {

	private $content;

	public function show() {
		global $db, $r;

		parent::show();

		if(!$this->checkAccess("editor")) rLocate(); //ak uzivatel nema dostatocne prava

		$id   = $r["arg"][3] + 0; // id spraovavaneho bloku


		$this->path[] = array(
		"admin/blocks",
		i18n::t("blocks")
		); //path


		/**
         * Vymazanie textoveho bloku podla id
         */

		if (($r["arg"][2] == "del") && ($id)) {

			$this->delete($id);
		}


		/**
         * Zmena pozicie bloku - posun hore alebo dole
         */

		if (in_array($r["arg"][2], array("up", "down"))) {

			$this->moveBlock($id);
		}


		/**
         * Editacia a pridavanie bloku
         */

		if (in_array($r["arg"][2], array("edit", "add"))) {

			/**
             * Spracovanie poslaneho formulara
             */

			if (($_POST["action"] == "change") || ($_POST["action"] == "add")) {

				$this->process($id);

			}


			/**
             * Zobrazenie formulara na pridavanie a editovanie bloku
             */

			// formular na pridanie
			if ($r["arg"][2] == "add") {

				$this->content = $this->addForm();


				// formular na editaciu
			} elseif ($r["arg"][2] == "edit") {

				$this->content = $this->editForm($id,$posted);

			}

			/**
             * Vytvorenie stylov podla adresarov a /gfx
             */
			$styles = array();
			foreach (rDirList("{$r["root"]}/gfx/blocks") as $f) {
				if (is_dir("{$r["root"]}/gfx/blocks/$f")) $styles[] = $f;
			}

			$this->tpl->assign("styles", $styles);
			$this->tpl->assign("content", $this->content);
			$this->tpl->assign("path", $this->path);
			$this->tpl->display("admin-blocks.tpl");
			return;
		}



		/**
         * Predvolena akcia - zobrazenie zoznamu blokov
         */

		$blocks = $db->getArrRow("
            SELECT * FROM blocks
            WHERE lang = '{$r["lang"]}'
            ORDER BY weight
        "); //informacie o vsetkyhc blokoch

		$this->tpl->assign("blocks", $blocks);
$this->tpl->assign("path", $this->path);
		$this->tpl->display("admin-blocks.tpl");

	}

	/**
     * Metoda na vymazanie bloku
     *
     * @param integer $id id spracovavaneho bloku
     */
	private function delete($id) {
		global $db;

		$db->query("
                DELETE FROM blocks
                WHERE id = '$id'
            "); // vymazanie bloku

		rLocate("admin/blocks"); //presmerovanie spat
		return;
	}

	/**
     * Metoda na zmenu pozicie bloku
     *
     * @param integer $id id spracovavaneho bloku
     */
	private function moveBlock($id) {
		global $db,$r;

		$block1 = $db->getRow("
                SELECT id, weight FROM blocks WHERE id = $id
            "); //id a vaha spracuvavaneho bloku

		if ($r["arg"][2] == "up") { //ak ide o posun hore

			$block2 = $db->getRow("
                    SELECT id, weight FROM blocks
                    WHERE weight < {$block1["weight"]}
                    ORDER BY weight DESC
                "); //prvy blok s mensou vahou

		} else { //posun dole

			$block2 = $db->getRow("
                    SELECT id, weight FROM blocks
                    WHERE weight > {$block1["weight"]}
                    ORDER BY weight
                "); //prvy blok s vacsou vahou

		}

		if ($block2) { //ak je kam posunut

			$db->query("
                    UPDATE blocks SET weight = {$block2["weight"]}
                    WHERE id = {$block1["id"]};

                    UPDATE blocks SET weight = {$block1["weight"]}
                    WHERE id = {$block2["id"]};
                "); //vymena vahy blokov

		}

		rLocate("admin/blocks"); //prtesmerovanie spat
		return;
	}

	private function process($id) {
		global $db,$r;

		/**
                 * Kontrola vstupu
                 */
		foreach (array("title", "text", "style") as $a) {
			if (!get_magic_quotes_gpc()) {
				$_POST[$a] = addslashes($_POST[$a]);
			}
			$_POST[$a] = trim($_POST[$a]);
		}
		$_POST["title"] = strip_tags($_POST["title"]);


		/**
                 * Chyby vo vstupe
                 */
		if ($_POST["title"] == "") {
		$error["sk"][] = i18n::t("c_title");
		}

		if ($_POST["text"] == "") {
		$error["sk"][] = i18n::t("c_text");
		}


		//ak sa vyskytli chyby
		if ($error) {
			$this->tpl->assign("error", array(
			 i18n::t("c_please") . " ".implode(", ", $error["sk"]) . "."
			));

			$this->content = $_POST;
			$posted = true;


			// ak je vsetko v poriadku
		} else {

			/**
                     * Uprava textoveho bloku
                     */

			if ($_POST["action"] == "change") {

				$db->query("
                            UPDATE blocks SET
                                title = '{$_POST["title"]}',
                                text  = '{$_POST["text"]}'
                            WHERE id = $id AND lang = '{$r["lang"]}';

                            UPDATE blocks SET
                                style = '{$_POST["style"]}'
                            WHERE id = $id;
                        ");


				/**
                     * Vlozenie noveho textoveho bloku
                     */

			} elseif ($_POST["action"] == "add") {

				$id = $db->getCell("
                            SELECT NEXTVAL('blocks_id_seq')
                        "); //nove id

				$weight = $db->getCell("
                            SELECT MAX(weight) FROM blocks
                        ") + 1; //nova vaha

				/**
                         * Ulozenie bloku vo vsetkych jazykoch
                         */
				$query = "";
				//foreach ($r["i18n"]["languages"] as $lang => $language) {
					$query .= "
                                INSERT INTO blocks (
                                    id, lang, weight, title, text, style
                                ) VALUES (
                                    $id,
                                    '{$r["lang"]}',
                                    $weight,
                                    '{$_POST["title"]}',
                                    '{$_POST["text"]}',
                                    '{$_POST["style"]}'
                                );
                            ";
				//}
				$db->query($query);

			}

			rLocate("admin/blocks"); //presmerovanie spet
			return;
		}
	}

	/**
     * Metoda na pripravu dat pre formular na pridavanie
     *
     * @return array pripravene data
     */
	private function addForm() {

		$this->path[] = array(
		"",
		i18n::t("blocks_new")
		); //path

		$this->content["add"] = true;

		return $this->content;
	}

	/**
     * Metoda na pripravu dat pre formular na editaciu
     *
     * @param integer id id spracovavaneho bloku
     * @param boolean posted priznak, ci bol formular odoslany
     * @return array pripravene data
     */
	private function editForm($id,$posted) {
		global $db,$r;

		// formular este nebol poslany
		if (!$posted) {
			$content = $db->getRow("
                        SELECT * FROM blocks
                        WHERE id = $id AND lang = '{$r["lang"]}'
                    ");
		}

		if (!$content) {
			rLocate("admin/blocks"); //presmerovanie spat
			return;
		}


		$this->path[] = array(
		"",
		$r["lang"] => $content["title"]
		);
		$this->path[] = array(
		"",
		i18n::t("blocks_edit")
		);

		return $content;
	}
}

?>
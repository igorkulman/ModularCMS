<?php
/**
 * Trieda obsluhujuca spravu editorov
 */
class AdminEditor extends Page {

	public function show() {
		global $db, $r;

		parent :: show();

		if(!$this->checkAccess("admin")) rLocate(); //ak uzivatel nema dostatocne prava

		$id = $r["arg"][3] + 0; // id editora

		$this->path[] = array (
			"admin/editor",
			i18n::t("editors")
		); //path

		/**
		 * Vymazanie spracovavaneho editora
		 */
		if (($r["arg"][2] == "del") && ($id)) {

			$this->delete($id);
			return;
		}

		/**
		* Pridanie a editacia editora
		*/
		if (in_array($r["arg"][2], array (
				"edit",
				"add"
			))) {

			// spracovanie poslania formulara

			/**
			 * Pridanie alebo zmenua bloku - spracovanie
			*/
			if (($_POST["action"] == "change") || ($_POST["action"] == "add")) {

				$this->process($id);

			}

			/**
			 * Zobrazenie formulara - pridavanie
			 */
			if ($r["arg"][2] == "add") {

				$content = $this->addForm($posted);
				$content[login] = $_POST[login];

				// for editation
			}
			elseif ($r["arg"][2] == "edit") {

				$content = $this->editForm($id, $posted);
				$content[login] = $_POST[login];

			}

			//$content = $_POST;

			$this->tpl->assign("content", $content);
			$this->tpl->display("admin-editor.tpl");
			return;
		}

		/**
		 * Predvolena akcia - zoznam vsetkych editorov
		 */

		$editors = $db->getArrRow("
		            SELECT * FROM users WHERE type='editor' ORDER BY id
		        ");

		$this->tpl->assign("path", $this->path);

		$this->tpl->assign("editors", $editors);
		$this->tpl->display("admin-editor.tpl");

	}

	/**
	 * Pirvatna metoda na zmazanie daneho editora
	 *
	 * @param integer $id id editora, ktory bude zmazany
	 */
	private function delete($id) {
		global $db;

		$db->query("
					DELETE FROM users WHERE id='$id' AND type='editor'
				");

		rLocate("admin/editor");
		return;
	}

	/**
	 * Metoda na pripravu dat pre formular na editaciu editora
	 *
	 * @param integer $id id editovaneho editora
	 * @param boolean $id priznak, ci uz bol formular odoslany
	 * @return array pripravene data
	 */
	private function editForm($id, $posted) {
		global $db, $r;

		if (!$posted) { //formular este nebol poslany
			$content = $db->getRow("
			                        SELECT * FROM users
			                        WHERE id = '$id' AND type='editor'
			                    "); //udaje o editorovi

		}

		if (!$content) {
			rLocate("admin/editor"); //presmerovanie spat ak neexistuje obsah
			return;
		}

		// path

		$this->path[] = array (
			"",
			i18n::t("editors_edit")
		);

		$this->tpl->assign("path", $this->path);

		return $content;
	}

	/**
	 * Metoda na spracovanie formulara - pridanie alebo zmena
	 *
	 * @param integer $id id srpacovavanej novinky
	 */
	private function process($id) {
		global $db, $r;

		foreach (array (
				"login",
				"passwd",
				"passwd2"
			) as $a) { //osetrenie vstupu
			if (!get_magic_quotes_gpc()) {
				$_POST[$a] = addslashes($_POST[$a]);
			}

			$_POST[$a] = trim($_POST[$a]); // odstranenie bielzch znakov
		}

		/**
		         * Kontrola chyb vstupu
		         */
		if ($_POST["login"] == "") {
			$error["sk"][] = i18n::t("c_login");
		}

		if ($_POST["passwd"] == "") {
			$error["sk"][] = i18n::t("c_passwd");;
		}

		if ($_POST["passwd2"] == "") {
			$error["sk"][] = i18n::t("c_passwd2");
		}

		if ($_POST["passwd"] != $_POST["passwd2"]) {
			$error["sk"][] = i18n::t("c_passwd_match");;
		}

		// ak sa vyskytli chyby
		if ($error) {
			$this->tpl->assign("error", array (
				i18n::t("c_please") . " ".implode(", ", $error["sk"]) . ".")); // chybove hlasenie

			$content = $_POST;
			$posted = true;

			$this->tpl->assign("content",$content);

			// ziadne chyby
		} else {

			/**
			         * Zmena editora
			         */
			if ($_POST["action"] == "change") {

				$pwd = md5($_POST["passwd"]);

				$db->query("
				                            UPDATE users SET
				                                login = '{$_POST["login"]}',
				                                text  = '$pwd'
				                            WHERE id = '$id' AND type='editor'
				                        "); //zmena editora

				/**
				     * Pridanie noveho editora
				     */
			}
			elseif ($_POST["action"] == "add") {

				$pwd = md5($_POST["passwd"]);

				$id = $db->getCell("
				                            SELECT NEXTVAL('users_id_seq')
				                        "); //nove id

				//pridanie
				$query = "
				                                INSERT INTO users (
				                                    id, login, type, enabled, lang, password
				                                ) VALUES (
				                                    $id,
				                                    '{$_POST["login"]}',
				                                    'editor',
				                                    'true',
				                                    'sk',
				                                    '$pwd'
				                                );
				                            ";

				$db->query($query); //vykonanie

			}

			rLocate("admin/editor"); //navrat spat
			return;
		}
	}

	/**
	 * Metoda na pripravu dat pre formular na pridanie noveho editora
	 *
	 * @param boolean posted priznak, ci bol formular uz odoslany
	 * @return array pripravene data
	 */
	private function addForm($posted) {

		$this->path[] = array (
			"",
			i18n::t("editors_new")
		); //path

		$content["add"] = true;

		$this->tpl->assign("path", $this->path);

		return $content;
	}
}
?>
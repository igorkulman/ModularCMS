<?php

/**
 * Trieda zobrazujuca administracne rozhranie
 *
 */
class Admin extends Page {

	/**
     * Modul pre administraciu pozadovanej sekcie
     *
     * @var string
     */
	public $adminPage;


	public function show() {
		global $db, $r;

		parent::show();

		// zistenie prav
		if (!$this->checkAccess("editor", true)) // ak nie je prihlaseny uzivatel = nema prava
		return; // koniec


		// uzivatel prihlaseny, vygenerovanie path
		$this->path[] = array(
		"admin",
		i18n::t("administration")
		);


		/**
         * Administracne moduly
         *
         * nazov => trieda
        */
		$adminModules = array(
			"content"           => "AdminContent",
			"inset-images"      => "AdminInsetImages",
			"blocks"            => "AdminBlocks",
			"news"              => "AdminNews",
			"editor"            => "AdminEditor",
			"config"            => "AdminConfig",
			"gallery"			=> "AdminGallery",
			"babel"				=> "AdminBabel",
			"files"				=> "AdminFiles",
			"catalogue"			=> "AdminCatalogue"
		);

		// ak nie je vybrany konkretny modul
		if (!isset($adminModules[$r["arg"][1]])) {
			$this->status(
			i18n::t("administration"),
			i18n::t("msg_a_menu")
			);
			return;
		}

		$this->adminPage = new $adminModules[$r["arg"][1]]; // vytvorenie pozadovaneho administracneho modulu

		$this->adminPage->path = $this->path; // nastavenie path

		$this->adminPage->show(); // zobrazenie pozadovaneho modulu
	}


	/**
     * Metoda volana pred zobrazenim triedy
     *
     */
	public function finish() {

		if (isset($this->adminPage)) { // ak ide o konkretny modul
			$this->adminPage->finish(); // pouzi ho


		} else { //inak
			parent::finish();
		}
	}
}

?>

<?php
/*
 * Fotogaleria
 *
 */
class Gallery extends Page {

	public function show() {
		global $r, $db;

		parent::show();

		$this->path[]=array(
				"gallery",
				i18n::t("gallery")
				);

		if (!empty($r["arg"][1]))
			$this->album($r["arg"][1]); //konkretny album
		else if ($r["arg"][2]=="foto")
			$this->foto($r["arg"][3]); //fotka
		else
			$this->albums();//listing


		$this->tpl->assign("path",$this->path);
		$this->tpl->display("gallery.tpl");
	}

	private function foto($album) {
		global $db, $r;

		$private=$popis=$db->getCell("SELECT private FROM gallery_albums AND lang='{$r["lang"]}' WHERE title='{$album}'");
		if($private=='t' && !$this->checkAccess("member")) rLocate();

		$this->path[]=array(
				"gallery/{$album}",
				$r["lang"] => parent::od_($album)
				);

		$this->path[]=array(
				"",
				i18n::t("photos")
				);

		$pager=self::pager(3,"gallery/{$album}/foto", 1, "gallery_fotos", "WHERE album_title='{$album}' AND lang='{$r["lang"]}'");

		$fotky=$db->getRow("SELECT * FROM gallery_fotos WHERE album_title='{$album}' AND lang='{$r["lang"]}'  ORDER BY weight {$pager["limit"]} ");

		$this->tpl->assign("foto", $fotky);
		$this->tpl->assign("action", "foto");
		$this->tpl->assign("album", $album);
	}

	/**
	 * Vypis fotiek konkretneho albumu
	 *
	 * @param unknown_type $album
	 */
	private function album($id) {
		global $db, $r;

		$data = $db->getRow("SELECT * FROM gallery_albums WHERE id={$id} AND lang='{$r[lang]}'");

		$this->path[]=array(
				"gallery/{$id}",
				$r["lang"] => $data[title]
			);

		$pager=self::pager(2,"gallery/{$id}", "gallery_images_fotos", "gallery_fotos", "WHERE album_id='{$id}' AND lang='{$r["lang"]}' ");


		$fotky=$db->getArrRow("SELECT * FROM gallery_fotos WHERE album_id='{$id}'  AND lang='{$r[lang]}' ORDER BY weight {$pager["limit"]} ");


		$this->tpl->assign("data", $data);
		$this->tpl->assign("fotos", $fotky);
		$this->tpl->assign("action", "album");
	}

	/**
	 * Zobrazenie vsetkych albumov vo fotogalerii
	 *
	 */
	private function albums() {
		global $db, $r;

		$where="WHERE lang='{$r[lang]}' AND visible=true";

		$pager=self::pager(1, "gallery", "gallery_images", "gallery_albums", $where);

		$albums=$db->getArrRow("SELECT id,title FROM gallery_albums {$where} ORDER BY id DESC {$pager["limit"]}");

		foreach($albums as &$item)
			$item[foto] = $db->getCell("SELECT link FROM gallery_fotos WHERE album_id='{$item[id]}' AND lang='{$r[lang]}' ORDER BY weight LIMIT 1");

		$this->tpl->assign("albums", $albums);
		$this->tpl->assign("action", "albums");
	}

	/**
	 * Metoda zabezpecujuca strankovanie
	 *
	 * @param integer $arg - cislo stranky
	 * @param string $pager
	 * @param string $pp
	 * @param string $table
	 * @param string $where
	 * @return array
	 */
	private function pager($arg, $pager, $pp, $table, $where = "") {
        global $db, $r;

        $page  = $r["arg"][$arg] + 0; if (!$page) $page = 1;
        if (!($pp+0)) $pp = $this->cfg["{$pp}_pp"] + 0;
        $limit = "LIMIT $pp OFFSET " . (($page-1) * $pp);

        $count = $db->getCell("SELECT COUNT(id) FROM $table " . ($where ? "$where" : ""));
        $pages = ceil($count / $pp);

        if ($page > 1) {
            $this->tpl->assign("prev", $page-1);
            $this->tpl->assign("first", 1);

        } if ($page < $pages) {
            $this->tpl->assign("next", $page+1);
            $this->tpl->assign("last", $pages);
        }
        $this->tpl->assign("pager", $pager);
        $this->tpl->assign("page",  $page);
        $this->tpl->assign("pages", $pages);
        $this->tpl->assign("count", $count);

        return array(
            "limit"     => $limit,
            "from"      => ($page-1) * $pp,
            "page"      => $page,
            "pages"     => $pages,
            "pp"        => $pp,
            "count"     => $count,
        );
    }
}


?>


<?php
/*
 * Trieda obsluhujuca preklady jazykovych konstant
 */
class AdminBabel extends Page {

	/**
     * Metoda na zobrazenie triedy
     *
     */
	public function show() {
		global $db, $r;

		parent::show();

		if(!$this->checkAccess("admin")) rLocate(); //ak uzivatel nema dostatocne prava`


		$this->path[] = array(
			"admin",
			i18n::t("babel")
		);

		if (!empty($_POST["lang"])) { //ide o ukladanie formulara

			$filename = "{$r["root"]}/i18n/{$_POST["lang"]}.txt";

			unset($_POST["lang"]);

			$string = "";

			foreach ($_POST as $key=>$value) { //formatovanie
				$string .= "{$key} = {$value}\n";
			}

			file_put_contents($filename, $string); //zapis do suboru
			i18n::init(); //reinicializacia

			$saved = true;
		}

		$langs=array();

		foreach (i18n::$translations as $key => $value)
			$langs[]=$key;

		if (empty($r["arg"][2])) //ak nie je nastaveny jazyk na preklad, presmerovanie na preklad prveho
			rLocate("admin/babel/{$langs[0]}");

		$lang = $r["arg"][2];


		$this->tpl->assign("path",$this->path);

		$this->tpl->assign("langs",$langs);
		$this->tpl->assign("saved",$saved);

		$this->tpl->assign("data",i18n::$translations[$lang]);
		$this->tpl->display("admin-babel.tpl");
	}

}
?>

<?php

/**
 * Trieda na administraciu noviniek
 *
 */
class AdminNews extends Page {

	private $content;

	/**
     * Metoda na zobrazenie textoveho obsahu
     *
     */
	public function show() {
		global $db, $r;

		parent::show();

		if(!$this->checkAccess("editor")) rLocate(); //ak uzivatel nema dostatocne prava

		$id   = $r["arg"][3] + 0; // id novinky pri spracovavani


		$this->path[] = array(
		"admin/news",
		i18n::t("news")
		); //path


		/**
         * Vymazanie spracovavanej novinky
         */
		if (($r["arg"][2] == "del") && ($id)) {

			$this->delete($id);
		}



		/**
        * Pridanie a editacia novinky
        */
		if (in_array($r["arg"][2], array("edit", "add"))) {

			// spracovanie poslania formulara

			/**
             * Pridanie alebo zmenua novinky - spracovanie
            */
			if (($_POST["action"] == "change") || ($_POST["action"] == "add")) {

				$this->process($id);

			}


			/**
             * Zobrazenie formulara - pridavanie
             */
			if ($r["arg"][2] == "add") {

				$this->content=$this->addForm($posted);


				// for editation
			} elseif ($r["arg"][2] == "edit") {

				$this->content=$this->editForm($id,$posted);

			}

			$this->tpl->assign("path", $this->path);

			$this->tpl->assign("content", $this->content);
			$this->tpl->display("admin-news.tpl");
			return;
		}



		/**
         * Predvolena akcia - zoznam vsetkych noviniek
         */

		$messages = $db->getArrRow("
            SELECT *, to_char(date, 'DD.MM.YYYY') AS d FROM news
            WHERE lang = '{$r["lang"]}'
            ORDER BY date DESC, id DESC
        ");

		$this->tpl->assign("path", $this->path);

		$this->tpl->assign("messages", $messages);
		$this->tpl->display("admin-news.tpl");

	}

	/**
     * Pirvatna metoda na zmazanie danej novinky
     *
     * @param integer $id id novinky, ktora bude zmazana
     */
	private function delete($id) {
		global $db, $r;

		$db->query("
                DELETE FROM news
                WHERE id = '$id'
            "); //zmazanie

		// navrat na novinky ak bola mazana /news
		if (($r["arg"][4] == "page") && ($page = $r["arg"][5] + 0)) {
			rLocate("news/$page");
			return;
		}

		//navrat do administracie ak islo o mazanie z /amdin/news
		rLocate("admin/news");
		return;
	}

	/**
     * Metoda na spracovanie formulara - pridanie alebo zmena
     *
     * @param integer $id id srpacovavanej novinky
     */
	private function process($id) {
		global $db, $r;


		foreach (array("title", "date", "text") as $a) { //osterenie vstupu
			if (!get_magic_quotes_gpc()) {
				$_POST[$a] = addslashes($_POST[$a]);
			}

			$_POST[$a] = trim($_POST[$a]); // odstranenie bielzch znakov
		}
		$_POST["title"] = strip_tags($_POST["title"]);

		$date  = eregi_replace('^(..)\.(..)\.(....)$', '\3-\2-\1', trim($_POST["date"])); // datum

		/**
                 * Kontrola chyb vstupu
                 */
		if ($_POST["title"] == "") {
		$error["sk"][] = "nadpis";
		$error["en"][] = "title";
		}

		if (($date == "") || (!ereg('^[0-9]{4}-[0-9]{2}-[0-9]{2}', $date))) {
		$error["sk"][] = "dátum";
		$error["en"][] = "date";
		}

		if ($_POST["text"] == "") {
		$error["sk"][] = "text";
		$error["en"][] = "text";
		}



		// ak sa vyskytli chyby
		if ($error) {
			$this->tpl->assign("error", array(
			"admin" => true,
			"sk"    => "Skontrolujte prosím " . implode(", ", $error["sk"]) . ".",
			"en"    => "Please check " . implode(", ", $error["en"]) . "."
			)); // chybove hlasenie

			$this->content = $_POST;
			$posted = true;


			// ziadne chyby
		} else {


			/**
                     * Zmena novinky
                     */
			if ($_POST["action"] == "change") {

				$db->query("
                            UPDATE news SET
                                title = '{$_POST["title"]}',
                                text  = '{$_POST["text"]}'
                            WHERE id = $id AND lang = '{$r["lang"]}';

                            UPDATE news SET
                                date = '$date'
                            WHERE id = $id AND lang = '{$r["lang"]}';
                        "); //zmena novinky


				/**
                     * Pridanie novej novinky
                     */
			} elseif ($_POST["action"] == "add") {

				$id = $db->getCell("
                            SELECT NEXTVAL('news_id_seq')
                        "); //nove id

				//pridanie novinky
				$query = "";
				//foreach ($r["i18n"]["languages"] as $lang => $language) {
					$query .= "
                                INSERT INTO news (
                                    id, lang, title, date, text
                                ) VALUES (
                                    $id,
                                    '{$r["lang"]}',
                                    '{$_POST["title"]}',
                                    '$date',
                                    '{$_POST["text"]}'
                                );
                            ";
				//}
				$db->query($query); //vykonanie

			}

			rLocate("admin/news"); //navrat spat
			return;
		}
	}

	/**
     * Metoda na pripravu dat pre formular na pridanie novej novinky
     *
     * @param boolean posted priznak, ci bol formular uz odoslany
     * @return array pripravene data
     */
	private function addForm($posted) {

		$this->path[] = array(
		"",
		i18n::t("news_new")
		); //path

		$this->tpl->assign("path", $this->path);

		$this->content["add"] = true;


		if (!$posted) {
			$this->content["d"] = date("d.m.Y"); //predvolene hodnoty
		}

		return $this->content;
	}


	/**
     * Metoda na pripravu dat pre formular na editaciu noviniek
     *
     * @param integer $id id editovanej novinky
     * @param boolean $id priznak, ci uz bol formular odoslany
     * @return array pripravene data
     */
	private function editForm($id,$posted) {
		global $db,$r;


		if (!$posted) { //formular este nebol poslany
			$this->content = $db->getRow("
                        SELECT *, to_char(date, 'DD.MM.YYYY') AS d FROM news
                        WHERE id = $id AND lang = '{$r["lang"]}'
                    "); //udaje o novinke

		}

		if (!$this->content) {
			rLocate("admin/news"); //presmerovanie spat ak neexistuje obsah
			return;
		}

		// path
		$this->path[] = array(
		"",
		$r["lang"] => $this->content["title"]
		);
		$this->path[] = array(
		"",
		i18n::t("news_edit")
		);

		$this->tpl->assign("path", $this->path);

		return $this->content;
	}

}

?>

<?php

/**
 * Trieda reprezentujuca tetxovy obsah
 *
 */
class Content extends Page {

	/**
     * Metoda na zobrazenie textoveho obsahu
     *
     */
	public function show() {
		global $db, $r;

		parent::show();



		if (!is_numeric($r["arg"][1])) {
			$id = $db->getCell("SELECT id FROM content WHERE alias='{$r["arg"][1]}' AND lang = '{$r["lang"]}'");
		} else
			$id = $r["arg"][1];


		// id obsahu
		$id   = $id + 0;

		// predvolena stranka - Home
		if (!$r["arg"][1]) { //ak neexsituje id
			$id = $db->getCell("SELECT * FROM content WHERE \"default\" = true AND lang = '{$r["lang"]}'") + 0; // nastavi sa ako id predvolena stranka
			$r["arg"][1] = $id;
		}


		// nacitanie stranky z DB podla id a jazyka
		$content = $db->getRow("SELECT * FROM content WHERE id = $id AND lang = '{$r["lang"]}'");


		// ak stranka nie je najdena
		if (!$content) {

			// zisti jazyky
			$languages = array();
			foreach ($db->getCol("SELECT lang FROM content WHERE id = $id") as $lang) {
				$languages[] = "<a href=\"" . rA("$lang/content/$id", true) . "\">{$r["i18n"]["languages"][$lang]}</a>";
			}

			if ($languages) {
				$this->status(
				i18n::t("not_found"),
				i18n::t("msg_other_lang")
				);

				// ak neexistuje v ziadnom jazyku
			} else {
				$this->status(
				i18n::t("not_found"),
				i18n::t("msg_not_found")
				);
			}

			return;
		}


		// aktivna polozka menu
		$this->active_menu = $db->getCell("SELECT id FROM menu WHERE link = 'content/$id'") + 0;

		if ($this->active_menu) { // ak ide o stranku v menu

			$content["menu_id"] = $this->active_menu;

			// cesta
			$tree = new rTree("menu", "id", "parent_id");
			$tree->setQuery("path", "
                SELECT
                    m.%PARENT_ID% AS %PARENT_ID%,
                    m.%ID% AS %ID%,
                    m.link,
                    m.title AS title,
                    c.id AS content_id
                FROM %TABLE% m
                LEFT JOIN content c ON 'content/' || c.id = m.link
                WHERE m.%ID% = %d AND m.lang = '{$r["lang"]}'
            ");
			$path = $tree->getPath(array("id" => $this->active_menu));


			foreach ($path as $p) {
				$this->path[] = array(
				$p["content_id"] ? "content/{$p["alias"]}" : "",
				$r["lang"] => $p["title"]
				); // vygeneruj cestu
			}

			// stranka nie je v menu
		} else {
			$this->path[] = array(
			"content/$id",
			$r["lang"] => $content["title"]
			);

		}

		if ($content["album_id"]+0>0) {

			$content["images"] = $db->getArrRow("SELECT * FROM gallery_fotos WHERE album_id='{$content["album_id"]}'  AND lang='{$r[lang]}' ORDER BY weight {$pager["limit"]} ");

		}

		//prevod odkazov
		$this->cleanUrl($content["text"]);
		$content["text"] = html_entity_decode($content["text"],ENT_QUOTES,"UTF-8");

		$this->tpl->assign("path", $this->path);

		$this->tpl->assign("content",     $content); // obsah na sablonu
		$this->tpl->assign("title",       $content["title"]); //nazov na sablonu

		$this->tpl->display("content.tpl"); // zobrazenie sablony
	}


}

?>

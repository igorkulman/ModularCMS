<?php
/**
 * Trieda generujúca mapu webu
 *
 */
class Sitemap extends Page {

    /**
     * Metóda na spracovanie a zobrazenie mapy webu
     *
     */
    public function show() {
        global $db, $r;

        parent::show(); // volanie predka

        // vytvorenie stromu menu
        $tree = new rTree("menu", "id", "parent_id");

        $this->map_html = "";

        foreach ($this->menu_positions as $position => $info) { //pre kaľdú pozíciu menu

            $tree->setQuery("tree", "
                SELECT t1.%ID%, t1.title, t1.link, COUNT(t2.%ID%) AS count
                FROM %TABLE% t1
                LEFT JOIN %TABLE% t2
                   ON t2.%PARENT_ID% = t1.%ID%
                  AND t2.lang = '{$r["lang"]}'
                WHERE t1.%PARENT_ID% = %d
                  AND t1.menu = '$position'
                  AND t1.lang = '{$r["lang"]}'
                GROUP BY t1.%ID%, t1.weight, t1.title, t1.link
                ORDER BY t1.weight, t1.%ID%
            "); // vytvorenie stromu


            $nodes = $tree->getMultiTree(0, $this->active_menu + 0, true); //strom menu

            $this->generateList($nodes); // vygenerovanie mapy
            $this->map_html .= "\n\n<br />\n\n";
        }

        $this->path[] = array(
            "",
            i18n::t("sitemap")
        ); //path

        // doplnena cesta
		$this->tpl->assign("path", $this->path);



        $this->status(
            i18n::t("sitemap"),

                $this->map_html

        ); //zobrazenie
    }


    /**
     * Metoda na vygenerovanie mapy zo stromu poloľiek
     *
     * @param array $nodes strom poloľiek
     * @return string mapa webu
     */
    private function generateList($nodes) {
        $this->map_html .= "<ul>\n"; //zoznam

        foreach ((array)$nodes as $node) { //pre kaľdú poloľku

            $this->map_html .= "<li><a href=\"" . rUrl($node["link"]) . "\"" .
                                ($this->active_menu == $node["id"] ? " class=\"selected\"" : "") .
                                ">{$node["title"]}</a>";

            if (count($node["children"])) //ak má podoloľky
            	$this->generateList($node["children"]);

            $this->map_html .= "</li>\n";
        }

        $this->map_html .= "</ul>\n"; //koniec zoznamu

        return $this->map_html;
    }


}

?>
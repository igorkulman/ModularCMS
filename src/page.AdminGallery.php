<?php
/**
 * Trieda AdminGallery na amdinistraciu fotogalerie
 *
 */
 class AdminGallery extends Page {

	/**
     * Metoda na zobrazenie triedy
     *
     */
 	public function show() {
 		global $r;

		parent::show();

 		if(!$this->checkAccess("admin")) rLocate(); //ak uzivatel nema dostatocne prava

 		$this->path[] = array(
            "admin/gallery",
            i18n::t("gallery")
        );

 		if($_POST["action"]) $action=$_POST["action"];
 			else $action=$r["arg"][2];


 		switch($action) {

 			// ALBUMY
 			case "addalbumform": {
 				$this->addAlbumForm();
 				break;
 			}	//formular na pridanie albumu

 			case "addalbum": {
 				$this->addAlbum();
 				break;
 			}	//pridanie noveho albumu


 			case "deletealbum": {
 				$this->deleteAlbum($r["arg"][3]);
 				break;
 			}   //zmazanie albumu

 			case "changealbumform": {
 				$this->changeAlbumForm($r["arg"][3]);
 				break;
 			}	//formular na zmenu albumu

 			case "changealbum": {
 				$this->changeAlbum($r["arg"][3]);
 				break;
 			}   //zmena albumu

 			case "albumphotos": {
 				$this->albumFotos($r["arg"][3]);
 				break;
 			} //detail albumu

 			//FOTKY
 			case "addfotoform": {
 				$this->addFotoForm();
 				break;
 			} // formular na nove fotky

 			case "addfoto": {
 				self::addFoto();
 				break;
 			} //pridanie fotiek

 			case "edit": {
 				self::editPhoto($r["arg"][3]);
 				break;
 			} //formular an editovanie fotky

 			case "editfoto": {
 				self::editPhotoDB();
 				break;
 			} //zmena fotky

 			case "top": {
 				self::topPhoto($r["arg"][3]);
 				break;
 			} //posun vyssie vramci albumu

 			case "down": {
 				self::downPhoto($r["arg"][3]);
 				break;
 			} //posun nizsie vramci albumu

 			case "del": {
 				self::delPhoto($r["arg"][3]);
 				break;
 			} //zamazanie fotky

 			//vypis albumov
 			case "main": {
 				$this->main;
 				break;
 			}

 			case "visible": {
 				$this->setVisible($r["arg"][3],$r["arg"][4]);
 				break;
 			}

 			default:
 				$this->main();


 		}

 		$this->tpl->assign("path",$this->path);
 		$this->tpl->display("admin-gallery.tpl");
 	}

 	/**
 	 * Zamazanie fotky podla id
 	 *
 	 * @param integer $id - ID mazanej fotky
 	 */
 	private function delPhoto($id) {
	    global $db,$r;

	    $data = $db->getRow("SELECT * FROM gallery_fotos WHERE id = $id AND lang='{$r[lang]}'");

	    //zamazanie fotiek z disku
	    unlink($r[root]."/data/photos/".$data[link].".jpg");
	    unlink($r[root]."/data/photos/".$data[link]."-thumb.jpg");

	    $db->query("DELETE FROM gallery_fotos WHERE id = '$id'");

	    rLocate("admin/gallery/albumphotos/{$data[album_id]}");
  	}

  	/**
  	 * Posunie fotku vyssie vramci album
  	 *
  	 * @param integer $id
  	 */
 	private function topPhoto($id) {
	    global $db,$r;


	    $item = $db->getRow("SELECT * FROM gallery_fotos WHERE id=$id AND lang='{$r[lang]}'");

	    $item2 = $db->getRow("SELECT * FROM gallery_fotos WHERE weight<{$item[weight]} AND lang='{$r[lang]}' ORDER BY weight DESC");



	    $db->query("UPDATE gallery_fotos SET weight={$item[weight]} WHERE id={$item2[id]}");
	    $db->query("UPDATE gallery_fotos SET weight={$item2[weight]} WHERE id={$item[id]}");



	    rLocate("admin/gallery/albumphotos/{$item[album_id]}");

	    //echo $album;

	    //return;

   }

    /**
     * Posunie polozku nizsie
     * - princip ako u topPhoto
     *
     * @param integer $id
     */
    private function downPhoto($id) {
	    global $db,$r;

	    $item = $db->getRow("SELECT * FROM gallery_fotos WHERE id=$id AND lang='{$r[lang]}'");

	    $item2 = $db->getRow("SELECT * FROM gallery_fotos WHERE weight>{$item[weight]} AND lang='{$r[lang]}' ORDER BY weight ASC");

	    $db->query("UPDATE gallery_fotos SET weight={$item[weight]} WHERE id={$item2[id]}");
	    $db->query("UPDATE gallery_fotos SET weight={$item2[weight]} WHERE id={$item[id]}");

	    rLocate("admin/gallery/albumphotos/{$item[album_id]}");



   }

   /**
    * Zmena fotky po editacii
    *
    */
 	private function editPhotoDB() {
	   	global $db,$r;

	   $query = "UPDATE gallery_fotos
	              SET descr = '{$_POST["desc"]}',
	              	  title = '{$_POST["title"]}'
	              WHERE id= '{$_POST["id"]}' AND lang='{$r["lang"]}'
	              ";

	  $db->query($query);

	  $album_id = $db->getCell("SELECT album_id FROM gallery_fotos WHERE id = '{$_POST["id"]}'");


	  rLocate("admin/gallery/albumphotos/$album_id");

   }

   /**
    * Editacia fotky
    *
    * @param integer $id - ID fotky
    */
 	private function editPhoto($id) {
	 	global $db,$r;


	    $foto=$db->getArrRow("SELECT * FROM gallery_fotos WHERE id='$id' AND lang='{$r["lang"]}'");

	    $this->tpl->assign("foto",$foto[0]);

	    $this->tpl->assign("action","editfotoform");
	    //$this->tpl->display("admin-gallery.tpl");

    return;

   }

   /**
    * Pripravi formular na pridanie albumu
    *
    */
 	private function addAlbumForm() {
 		$this->path[] = array(
                    "",
                    i18n::t("gallery_newalbum")
                );

 		$this->tpl->assign("action", "addalbumform");
 	}

 	/**
 	 * Formular na pridavanie novych fotiek
 	 *
 	 */
 	private function addFotoForm() {
 		global $db, $r;

 		$this->path[] = array(
                    "",
                    i18n::t("gallery_addphotos")
                );

 		$albumy=$db->getArrRow("SELECT id,title FROM gallery_albums WHERE lang='{$r["lang"]}'");

		$this->tpl->assign("langs",$r["i18n"]["languages"]);

 		$this->tpl->assign("albums", $albumy);
 		$this->tpl->assign("action", "addfotoform");
 	}

	/**
	 * Pridanie noveho albumu
	 */
 	private function addAlbum() {
 		global $db, $r, $ftp;

 		$id = $db->getCell("SELECT NEXTVAL('gallery_albums_id_seq')");
 		$weight = $db->getCell("SELECT MAX(weight) FROM gallery_albums") + 1;


        //vlozenie albumu pre vsetky jazyky
        foreach ($r["i18n"]["languages"] as $lang => $languages) {

	 		$db->query("INSERT INTO gallery_albums(id, title, text, lang, weight)
	 				   VALUES ('{$id}', '{$_POST["title_{$lang}"]}', '{$_POST["text_{$lang}"]}', '{$lang}', '$weight')
	 				   ");
        }

        rLocate("admin/gallery");
 	}

	/**
	 * Pridanie fotky
	 */
 	private function addFoto() {
 		global $db, $r, $ftp;

		$name="";


		for ($pc=0; $pc<count($_FILES["image"]["name"]); $pc++) {

			if($_FILES["image"]["name"][$pc]) {
				$this->uploadPhoto($pc, "data/photos", "", true, "gallery_images", true, $name);

				$id = $db->getCell("SELECT NEXTVAL('gallery_fotos_id_seq')");
				$weight = $db->getCell("SELECT MAX(weight) FROM gallery_fotos") + 1;

				foreach ($r["i18n"]["languages"] as $lang => $languages) {

					$db->query("INSERT INTO gallery_fotos(id, album_id, link, title, descr, weight, lang)
								VALUES('{$id}', '{$_POST["album"]}', '{$name}', '{$_POST["title"]["$lang"][$pc]}','{$_POST["desc"]["$lang"][$pc]}',$weight, '$lang')");
				}

			}
		}
		//return;


 		rLocate("admin/gallery/albumphotos/{$_POST["album"]}");
 	}

 	/**
 	 * Metoda na zmazanie albumu
 	 *
 	 * @param integer $id - id albumu
 	 */
 	private function deleteAlbum($id) {
 		global $db, $r;


 		$fotos = $db->getArrRow("SELECT * FROM gallery_fotos WHERE album_id='{$id}' AND lang='{$r[lang]}'");

 		$db->query("DELETE FROM gallery_fotos WHERE album_id='{$id}'");
 		$db->query("DELETE FROM gallery_albums WHERE id='{$id}'");

		foreach ($fotos as &$item) {
			  unlink($r[root]."/data/photos/".$item[link].".jpg");
	   		  unlink($r[root]."/data/photos/".$item[link]."-thumb.jpg");
		}

		rLocate("admin/gallery");
 	}

	/**
	 * Formular na editaciu albumu
	 *
	 * @param int $id id albumu
	 */
 	private function changeAlbumForm($id) {
 		global $db, $r;

 		$this->path[] = array(
                    "",
                    i18n::t("gallery_album_edit")
                );

 		$change=$db->getRow("SELECT * FROM gallery_albums WHERE id={$id} AND lang='{$r["lang"]}'");

 		$this->tpl->assign("album", $change);
 		$this->tpl->assign("action", "changealbumform");
 	}

	/**
	 * Zmena detailov albumu
	 *
	 * @param int $id id albumu
	 */
 	private function changeAlbum($id) {
 		global $db, $r;

 		$db->query("UPDATE gallery_albums
 					SET title='{$_POST["title"]}',
 					text='{$_POST["text"]}'
 					WHERE id='{$id}' AND lang='{$r["lang"]}'
 				   ");

 		rLocate("admin/gallery");
 	}


	/**
	 * Zobrazenie fotiek daneho albumu
	 *
	 * @param int $id id albumu
	 */
 	private function albumFotos($id) {
 		global $db, $r;

 		$this->path[] = array(
            "admin/gallery/albumphotos/{$id}",
            i18n::t("gallery")
        );




 		$fotos=$db->getArrRow("SELECT * FROM gallery_fotos WHERE album_id={$id} AND lang='{$r["lang"]}' ORDER BY weight");


 		$albumy=$db->getCol("SELECT title FROM gallery_albums WHERE lang='{$r["lang"]}'");

 		$this->tpl->assign("AdminGal_fotos", $fotos);


 		$this->tpl->assign("album_title", $db->getCell("SELECT title FROM gallery_albums WHERE id={$r["arg"][3]} AND lang='{$r[lang]}' "));
 		$this->tpl->assign("action", "albumfotos");
 		$this->tpl->assign("albums", $albumy);
 		$this->tpl->assign("ktory", $r["arg"][3]);
 	}


 	/**
 	 * Administratoirsky vypis albumov
 	 *
 	 */
 	public function main() {
 		global $db, $r;

 		$albums=$db->getArrRow("SELECT * FROM gallery_albums WHERE lang='{$r["lang"]}' ORDER BY weight");

 		$this->tpl->assign("AdminGal_albums", $albums);
 	}

 	/**
 	 * Nastavenie viditelnosti albumu
 	 *
 	 * @param string $flag priznak
 	 * @param int $id id albumu
 	 */
 	private function setVisible($flag,$id) {
 		global $db;

		$sql = "UPDATE gallery_albums
				SET visible = '{$flag}'
				WHERE id={$id}
		";

		$db->query($sql);

		rLocate("admin/gallery");
 	}





 }
?>

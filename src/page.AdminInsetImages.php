<?php

/**
 * Modul na spravu obrazkov vkladanych do obsahu
 *
 */
class AdminInsetImages extends Page {

	/**
     * Metoda na zobrazenie modulu a spracovanie udalosti
     *
     */
	public function show() {
		global $r;

		parent::show();

		if(!$this->checkAccess("editor")) rLocate(); //ak uzivatel nema dostatocne prava

		$page_id = "page";

		$dir    = $r["root"]    . "/data/content/inset_images/$page_id"; // adresar na ukladanie obrazkov
		$webDir = $r["webRoot"] . "/data/content/inset_images/$page_id";

		$page   = $r["arg"][3] + 0;
		if (!$page)
		$page = 1; // stranka pre pager

		// javascript pre wysiwyg editor
		if ($r["arg"][3] == "list") {

			$files = rDirList($dir, array('\.jpg$'), array('thumb')); // zoznam miniatur
			$images = array();

			foreach ($files as $f) {
				$images[] = array("name" => $f, "url" => "$webDir/$f"); //vytvorenie pola obrazkov
			}

			$this->tpl->assign("path", $this->path);

			$this->tpl->assign("images", $images); // priradenie do sablony
			$this->tpl->display("admin-inset-images-list.tpl"); // zobrazenie

			return;
		}


		// zmazanie obrazka
		if (($r["arg"][4] == "del") && ($id = $r["arg"][5] + 0)) {

			$this->delete($id,$dir,$page_id,$page);
		}


		// upload obrazkov
		if ($_POST["action"] == "add") {

			$this->addImages($dir,$page_id,$page);
		}


		// vypis a formular
		$files = rDirList($dir, array('-thumb\.jpg$')); // zoznam miniatur


		$pp    = $this->cfg["inset_images_pp"]; // pocet obrazkov na stranu

		$count = count($files); // pocet obrazkov
		$pages = ceil($count / $pp); // pocet stran

		$j = ($page-1) * $pp + 1;
		$images = array();

		for ($i = ($page-1) * $pp; $i < $page * $pp; $i++) {
			if (!$files[$i])
			break;
			$images[$j++] = eregi_replace('-thumb\.jpg$', "", $files[$i]);
		}

		if ($page_id == "page") {
			$this->path[] = array(
			"admin/content",
			i18n::t("contents")
			);
			$this->path[] = array(
			"",
			i18n::t("content_insetimages")
			);

		}

		/**
         * Vygenerovanie pagera
         */

		if ($page > 1) {
			$this->tpl->assign("prev", $page-1);
			$this->tpl->assign("first", 1);

		} if ($page < $pages) {
			$this->tpl->assign("next", $page+1);
			$this->tpl->assign("last", $pages);
		}

		$this->tpl->assign("pager", "admin/inset-images/$page_id");
		$this->tpl->assign("page",  $page);
		$this->tpl->assign("pages", $pages);
		$this->tpl->assign("count", $count);

		$this->tpl->assign("input", $this->cfg["inset_images_upload_count"] + 1);

		$this->tpl->assign("images",  $images);
		$this->tpl->assign("dir",     $webDir);
		$this->tpl->assign("pageId",  $page_id);

		$this->tpl->assign("path", $this->path);

		$this->tpl->display("admin-inset-images.tpl");
	}

	/**
     * Metoda na zmazanie obrazku
     *
     * @param integer $id id spracovavaneho obrazka
     */
	private function delete($id,$dir,$page_id,$page) {

		$files = rDirList($dir, array('-thumb\.jpg$'));

		$thumb = $files[$id-1];
		$image = eregi_replace('-thumb', '', $thumb);

		unlink("$dir/$thumb"); //zmazanie nahladu
		unlink("$dir/$image"); //a obrazka

		rLocate("admin/inset-images/$page_id/$page"); //presmerovanie spat

		return;
	}

	private function addImages($dir,$page_id,$page) {

		foreach ($_FILES["image"]["name"] as $id => $name) {
			if ($name)
			$this->uploadPhoto($id, $dir, null, true, "inset_images", true); // funkcia zdedena od Page

		}

		rLocate("admin/inset-images/$page_id/$page"); // presmerovanie spet
		return;
	}


}

?>
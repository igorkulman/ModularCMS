<?php

/**
 * Trieda na zobrazenie noviniek
 *
 */
class News extends Page {

/**
     * Metoda na zobrazenie triedy
     *
     */
	public function show() {
		global $db, $r;

		parent::show();

		$this->path[] = array(
		"news",
		i18n::t("news")
		); //path

		$this->active_menu = $db->getCell("
            SELECT id FROM menu
            WHERE link = 'news' AND lang = '{$r["lang"]}'
        "); //aktivne menu


		/**
         * Zobrazenie noviniek
         */
		$where = "lang = '{$r["lang"]}'";

		$pager = $this->pager(1, "news", "news", "news", $where); //rozdelenie na strany

		$messages = $db->getArrRow("
            SELECT *, to_char(date, 'DD.MM.YYYY') AS d FROM news
            " . ($where ? "WHERE $where" : "") . "
            ORDER BY date DESC, id DESC
            {$pager["limit"]}
        "); // novinky


		foreach ($messages as &$msg) {

			$this->cleanUrl($msg[text]); //prevod odkazov
		}

		// doplnena cesta
		$this->tpl->assign("path", $this->path);

		$this->tpl->assign("messages", $messages);
		$this->tpl->display("news.tpl");

	}


	/**
     * Metoda na rozdelenie noviniek na viacero stran podla poctu
     *
     * @param string $arg umiestenie cisla aktualnej strany
     * @param string $pager prefix URL, ktore budu vygenerovane
     * @param string/integer $pp pocet noviniek na 1 stranu alebo premenna v konfugiracnom subore
     * @param string $table nazov tabulky s novinkami
     * @param string $where podmienka pre select
     * @return array udaje pre strankovanie
     */
	private function pager($arg, $pager, $pp, $table, $where = "") {
		global $db, $r;

		/**
         * Aktualna strana, predvolena = 1
         */
		$page  = $r["arg"][$arg] + 0;
		if (!$page)
		$page = 1;

		/**
         * Ak $pp nie je cislene, udaj z konfiguracneho suboru
         */
		if (!($pp+0))
		$pp = $this->cfg["{$pp}_pp"] + 0;

		$limit = "LIMIT $pp OFFSET " . (($page-1) * $pp); //limit pre novinky v databaze

		$count = $db->getCell("SELECT COUNT(id) FROM $table " . ($where ? "WHERE $where" : "")); //pocet noviniek

		$pages = ceil($count / $pp); //pocet stran

		if ($page > 1) { //ak nejde o pravu stranu
			$this->tpl->assign("prev", $page-1); //linka na predchadzajuju
			$this->tpl->assign("first", 1);      // na prvu

		} if ($page < $pages) { //ak nejde o poslednu stranu
			$this->tpl->assign("next", $page+1); // linka na nasledujucu
			$this->tpl->assign("last", $pages);  // na poslednu
		}
		/**
         * Uzitocne premnene
         */
		$this->tpl->assign("pager", $pager);
		$this->tpl->assign("page",  $page);
		$this->tpl->assign("pages", $pages);
		$this->tpl->assign("count", $count);

		return array(
		// limit
		"limit"     => $limit,
		// cislo prvej novinky na strane
		"from"      => ($page-1) * $pp,
		// aktualne cislo strany
		"page"      => $page,
		// celkovy pocet stran
		"pages"     => $pages,
		// pocet noviniek na strane
		"pp"        => $pp,
		// celkovy pocet
		"count"     => $count,
		);
	}
}

?>

<?php

/**
 * Trieda zabezpecujuca zmeny konfiguracneho suboru tpl/setup.conf
 *
 */
class AdminConfig extends Page {

	public function show() {
		global $db, $r;

		parent::show();

		if(!$this->checkAccess("admin")) rLocate(); //ak uzivatel nema dostatocne prava

		if (!empty($_POST[action])) {

			$cfg = $this->getConfig2();

			//rPrint($cfg);die();


			// aktualizacia nastaveni
			foreach ($_POST as $key => $value) {
				if ($key!="action")
					$cfg[$key]=$cfg[$key][0]." = ".$value;
			}

			//uprava komentarov a prazdnych riadkov
			foreach ($cfg as &$item) {

				if (is_array($item))
					$item=implode("",$item);

			}

			//rPrint($cfg); return;


			//zapis do suboru
			$fp=fopen($r["root"]    . "/tpl/conf/setup.conf",w);

			//	foreach ($cfg as $item)
					fwrite($fp,implode("\n",$cfg));

			fclose($fp);

			rLocate("admin/config");

		}

		$this->path[] = array(
		"",
		i18n::t("configuration")
		); //path

		$this->tpl->assign("path", $this->path);
		$this->tpl->assign("config",$this->getConfig($comment));

		//rPrint($comment);

		$this->tpl->assign("comment",$comment);
		$this->tpl->display("admin-config.tpl");




	}


	/**
	 * Funkcia na nacitanie konfiguracneho suboru
	 */
	private function getConfig(&$comment) {
		global $r;

		$dir    = $r["root"]    . "/tpl/conf"; //adresa tpl/conf

		$config = file($dir."/setup.conf");

		$cfg=array();
		$comment=array();

		for ($i=0;$i<count($config);$i++) {

		  $config[$i]=trim($config[$i]);

		  if (!preg_match("/^;/",$config[$i])) {
			$cfg[$i] = explode('=',$config[$i]);
			$cfg[$i][0]=trim($cfg[$i][0]);
			$cfg[$i][1]=trim($cfg[$i][1]);
			//$cfg[$i][2]=trim($cfg[$i][2]);
		  } else {
		  	$comment[$i]=explode('=',$config[$i]);
		  	$comment[$i][0]=trim(trim($comment[$i][0]),";");
		  	$comment[$i][1]=trim(trim($comment[$i][1]),";");
		  }

		}

		return $cfg;

	}

	private function getConfig2() {
		global $r;

		$dir    = $r["root"]    . "/tpl/conf"; //adresa tpl/conf

		$config = file($dir."/setup.conf");

		$cfg=array();
		$comment=array();

		for ($i=0;$i<count($config);$i++) {

		  $config[$i]=trim($config[$i]);

		  if (!preg_match("/^;/",$config[$i])) {
			$cfg[$i] = explode('=',$config[$i]);
			$cfg[$i][0]=trim($cfg[$i][0]);
			$cfg[$i][1]=trim($cfg[$i][1]);
			//$cfg[$i][2]=trim($cfg[$i][2]);
		  } else {
		  	$cfg[$i]=$config[$i];
		  }

		}

		return $cfg;

	}

}
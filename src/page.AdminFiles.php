<?php
/**
 * Trieda na spravu priradenych suborov
 */
class AdminFiles extends Page {

	public function show() {
		global $db, $r;

		parent::show();

		$this->path[] = array(
		"admin",
		i18n::t("content_files")
		);

		$dir    = $r["root"]    . "/data/files"; //cielovy adresar

		$files = rDirList($dir);

		if(!$this->checkAccess("editor")) rLocate(); //ak uzivatel nema dostatocne prava

		if ($r["arg"][2]=="delete") {

			$file = $files["{$r["arg"][3]}"];
			unlink($dir."/".$file);
			rLocate("admin/files");
			return;
		}

		if ($_POST["action"]=="add") {

			Page::UploadAllFiles("file",$dir);
			rLocate("admin/files");
			return;
		}

		$this->tpl->assign("path", $this->path);

		$this->tpl->assign("data",$files);
		$this->tpl->display("admin-files.tpl");
	}
}
?>
